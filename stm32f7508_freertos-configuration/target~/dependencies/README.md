# Overview

This pack provides the `fs` foundation library.

# Usage

Add the following line to your `module.ivy`:

	<dependency org="com.microej.pack" name="fs" rev="5.1.2"/>

# Requirements

This library requires the following Foundation Libraries:

	

# Dependencies

_All dependencies are retrieved transitively by MicroEJ Module Manager_.

# Source

N/A

# Restrictions

None.

---
_Copyright 2015-2020 MicroEJ Corp. All rights reserved._  
_MicroEJ Corp. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms._