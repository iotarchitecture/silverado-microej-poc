## 5.1.2 (2020-11-05)

Bugfixes:

  - M0074FS-168: Fix wrong packaging.

## 5.1.1 (2020-10-09)

Bugfixes:

  - M0074FS-167: Don't embed HIL classes in mock jar.

## 5.1.0 (2020-05-27)

Features:
	
  - M0074FS-83: add flush native function
	
## 5.0.1 (2019-09-27)

Bugfixes:

  - M0074FS-132: Fix workbench extension.
  - M0074FS-137: Fix LLAPI documentation.

## 5.0.0 (2019-05-31)

Features:

  - M0074FS-114: Update pack to use SNI 1.3.

## 4.0.2 (2018-07-19)

Bugfixes:
  
  - M0074FS-2: Generic FS pack.
  
## 4.0.1 (2018-03-23)

Bugfixes:

  - WI 20743: HIL: SNI.toCString() doesn't flush content when FS pack is embedded
  
## 4.0.0 (2018-01-30)

Features:    
    
  - WI 20393: Build pack compatible with MicroEJ Architecture 7.0.0
  - WI 18524: Build of a dev pack is sufficient
  - WI 20672: Fetch XPFP dependencies in a private conf
  
## 3.0.2 (2017-06-02)

Bugfixes:

  - WI19876: XPFP FS 3.0.1 wrong properties in release_pack.properties

## 3.0.1 (2017-06-01)

Bugfixes:

  - WI19365: No Java lib version in CCO ivy module name
  - WI19857: FS HIL UnsatisfiedLinkError when Unix filesystem used
  - WI19862: FS wrong canonicalPath returned by HIL UnixFileSystem implementation

## 3.0.0 (2017-03-07)

Features:    
    
  - Remove S3 System.out redirection patch
  
Bugfixes:

  - WI 17014: Missing fragment description in MicroEJ SDK 4 .platform -> content view -> details -> description
  - WI 19262: Missing C header files metadata in FS impl artifacts

## 2.2.1 (2016-12-30)

Features:    
    
  - Add an integration testsuite.

## 2.2.0 (2016-07-05)

Features:    
    
  - Add support for Unix and WinNT filesystems.

## 1.0.1 (2015-09-23)

Bugfixes:    
    
  - Fix issues when closing a file stream several times.

## 1.0.0  (2015-09-18)

Features:

  - Initial revision.

___  
_Copyright 2015-2020 MicroEJ Corp. All rights reserved._  
_MicroEJ Corp. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms._