<!--
	Markdown
	
	Copyright 2014 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

# Description
Add generic connection framework library (ECOM): connection, stream, datagram and devices management.

# References
  * Platform reference manual