<!--
	Markdown
	
	Copyright 2014 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

# Description
Add serial communication library (ECOM COMM): comm connection (UART).

# Dependencies
  * ECOM
  
# Configuration
  * Optional "ecom-comm/ecom-comm.xml" file (required to enable board's comm ports and include ECOM Comm low level API in BSP project).

# References
  * Platform reference manual