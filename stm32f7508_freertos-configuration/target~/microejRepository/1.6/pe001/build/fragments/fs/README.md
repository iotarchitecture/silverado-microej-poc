<!--
	Markdown
	Copyright 2014-2016 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

# Overview
This library contains classes and interfaces stubs of File System API (ESR025).

# Usage
Add the following line to your `module.ivy` or your `ivy.xml`:
> `<dependency org="ej.api" name="fs" rev="2.+"/>`

# Requirements
  - EDC-1.2 or higher
 
# Dependencies
None.

# Source
N/A

# Restrictions
None.