<!--
	Markdown
	Copyright 2014-2016 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

## 2.0.6 (2016-12-09)
Features:
  - WI18408 Update readme.

## 2.0.5 (2016-04-07)
Features:
  - Initial public revision.
