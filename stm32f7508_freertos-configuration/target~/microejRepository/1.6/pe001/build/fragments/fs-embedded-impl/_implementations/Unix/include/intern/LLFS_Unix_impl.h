/*
 * C
 *
 * Copyright 2016-2017 IS2T. All rights reserved.
 * IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 * @file
 * @brief MicroEJ FS Unix low level API
 * @author MicroEJ Developer Team
 * @version 2.0.0
 * @date 5 June 2019
 */

#define LLFS_Unix_IMPL_canonicalize Java_com_is2t_java_io_UnixFileSystem_canonicalizeNative
