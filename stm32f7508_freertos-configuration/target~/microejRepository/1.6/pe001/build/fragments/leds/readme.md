<!--
	Markdown
	
	Copyright 2014 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

# Description
Add MicroUI LEDs management library. Include low level API (.h) for the 
associated BSP project.
 
# Dependencies
  * MicroUI

# References
  * Embedded UI extension reference manual