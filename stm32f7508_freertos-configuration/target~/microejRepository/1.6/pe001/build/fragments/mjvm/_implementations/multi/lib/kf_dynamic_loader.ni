<!--
	Natives Interface
	
	Copyright 2013-2018 IS2T. All rights reserved.
	Modification and distribution is permitted under certain conditions.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->
<nativesInterface>

	<nativesPool name="com.is2t.kf.KFDynamicLoaderNativesPool">
		<native name="com.is2t.kf.DynamicLoaderNatives.allocate(int,int)long"/>
		<native name="com.is2t.kf.DynamicLoaderNatives.copy(long,byte[])boolean"/>
		<native name="com.is2t.kf.DynamicLoaderNatives.copyFrom(long,byte[],int,int,int)boolean"/>
		<native name="com.is2t.kf.DynamicLoaderNatives.copyTo(long,int,byte[],int,int)boolean"/>
		<native name="com.is2t.kf.DynamicLoaderNatives.free(long)void"/>
		<native name="com.is2t.kf.DynamicLoaderNatives.getSectionStartAddress(long)long"/>
		<native name="com.is2t.kf.DynamicLoaderNatives.readU4(long,int,boolean)int"/>
		<native name="com.is2t.kf.DynamicLoaderNatives.writeU4(long,int,boolean,int)void"/>
		<native name="com.is2t.kf.DynamicLoaderNatives.unlink(ej.kf.Feature)boolean"/>
	</nativesPool>
	
</nativesInterface>