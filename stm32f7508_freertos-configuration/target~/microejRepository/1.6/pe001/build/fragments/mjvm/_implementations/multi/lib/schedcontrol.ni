<!--
	Natives Interface
	
	Copyright 2014 IS2T. All rights reserved.
	Modification and distribution is permitted under certain conditions.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->
<nativesInterface>

	<nativesPool name="com.is2t.microjvm.mowana.threads.schedcontrol.ScheduleControllerStruct">
		<native name="com.is2t.schedcontrol.ScheduleController.getContext(java.lang.Thread)com.is2t.schedcontrol.ScheduleContext"/>		
		<native name="com.is2t.schedcontrol.ScheduleController.setEnabledMonitoringCounters(boolean)void"/>		
		<native name="com.is2t.schedcontrol.ScheduleController.setEnabledContextsCleanup(boolean)void"/>		
		<native name="com.is2t.schedcontrol.ScheduleController.resetQuantumOfAllContexts()void"/>		
	</nativesPool>
	
	<nativesPool name="com.is2t.microjvm.mowana.threads.schedcontrol.ScheduleContextStruct">
		<native name="com.is2t.schedcontrol.ScheduleContext.addThread(java.lang.Thread)com.is2t.schedcontrol.ScheduleContext"/>		
	</nativesPool>
	
	
	
</nativesInterface>