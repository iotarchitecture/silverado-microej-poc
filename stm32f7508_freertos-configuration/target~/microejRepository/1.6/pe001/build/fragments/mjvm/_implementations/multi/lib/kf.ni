<!--
	Natives Interface
	
	Copyright 2013-2018 IS2T. All rights reserved.
	Modification and distribution is permitted under certain conditions.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->
<nativesInterface>

	<nativesPool name="com.is2t.kf.KFNativesPool">
		<native name="com.is2t.kf.KernelNatives.initialize()boolean"/>
		<native name="com.is2t.kf.KernelNatives.getMaxNbInstalledFeatures()int"/>
		<native name="com.is2t.kf.KernelNatives.getInstalledFeatureDescriptor(int)long"/>
		<native name="com.is2t.kf.KernelNatives.getData(long)com.is2t.kf.ModuleData"/>
		<native name="com.is2t.kf.KernelNatives.loadStaticFeatureByName(char[],int)long"/>
		<native name="com.is2t.kf.KernelNatives.getEntryPointClass(ej.kf.Feature)java.lang.Class"/>
		<native name="com.is2t.kf.KernelNatives.setOwner(java.lang.Object,ej.kf.Feature)void"/>
		<native name="com.is2t.kf.KernelNatives.getOwner(java.lang.Object)ej.kf.Module"/>
		<native name="com.is2t.kf.KernelNatives.getContextOwner()ej.kf.Module"/>
		<native name="com.is2t.kf.KernelNatives.enter()void"/>
		<native name="com.is2t.kf.KernelNatives.exit()void"/>
		<native name="com.is2t.kf.KernelNatives.execClinitWrapper(ej.kf.Feature)void"/>
		<native name="com.is2t.kf.KernelNatives.linkToNative(ej.kf.Feature,long)void"/>
		<native name="com.is2t.kf.KernelNatives.unlinkToNative(ej.kf.Feature)void"/>
		<native name="com.is2t.kf.KernelNatives.loadFeatureByIndex(long,int)long"/>
		<native name="com.is2t.kf.KernelNatives.canLoadDynamicFeature()boolean"/>
		<native name="com.is2t.kf.KernelNatives.checkKernelSignature(byte[])boolean"/>
		<native name="com.is2t.kf.KernelNatives.kill(ej.kf.Feature)void"/>
		<native name="com.is2t.kf.KernelNatives.clone(java.lang.Object,ej.kf.Feature)java.lang.Object"/>
		<native name="com.is2t.kf.KernelNatives.bind(java.lang.Object,java.lang.Class)ej.kf.Proxy"/>
		<native name="com.is2t.kf.KernelNatives.newAnonymousProxy(java.lang.Object,ej.kf.Feature)ej.kf.Proxy"/>
		<native name="com.is2t.kf.KernelNatives.areEquivalentSharedInterfaces(java.lang.Class,java.lang.Class)boolean"/>
		<native name="com.is2t.kf.KernelNatives.isSharedInterface(java.lang.Class)boolean"/>
		<native name="com.is2t.kf.KernelNatives.isAPI(java.lang.Class)boolean"/>
		<native name="com.is2t.kf.KernelNatives.getImplementedSharedInterface(java.lang.Class,java.lang.Class)java.lang.Class"/>
		<native name="com.is2t.kf.KernelNatives.getTargetSharedInterface(java.lang.Class,java.lang.Class,ej.kf.Feature)java.lang.Class"/>
		<native name="com.is2t.kf.KernelNatives.setStarted(ej.kf.Feature)void"/>
		<native name="com.is2t.kf.KernelNatives.getState(ej.kf.Feature)int"/>
		<native name="com.is2t.kf.KernelNatives.getCriticality(ej.kf.Feature)int"/>
		<native name="com.is2t.kf.KernelNatives.setCriticality(ej.kf.Feature,int)void"/>
		<native name="com.is2t.kf.KernelNatives.getNextStoppedFeatureToUpdate()ej.kf.Feature"/>
	</nativesPool>
	
</nativesInterface>