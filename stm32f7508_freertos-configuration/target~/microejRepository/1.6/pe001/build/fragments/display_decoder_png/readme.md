<!--
	Markdown
	
	Copyright 2014 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

# Description
Add the PNG runtime decoder library.

# Dependencies
  * MicroUI
  * Display

# References
  * Embedded UI extension reference manual