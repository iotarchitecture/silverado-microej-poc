# Description
Display stack implementation with the following configuration:
  * BPP = 8: A pixel is represented on 8 bits maximum.
  * Layout = line: line by line.
