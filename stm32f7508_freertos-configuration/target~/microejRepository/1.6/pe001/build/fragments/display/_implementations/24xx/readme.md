# Description
Display stack implementation with the following configuration:
  * BPP = 24: A pixel is represented on 24 bits maximum.
  * Layout = line: line by line.
