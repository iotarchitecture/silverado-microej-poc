# Description
Display stack implementation with the following configuration:
  * BPP = 2: A pixel is represented on 2 bits maximum.
  * Layout = column: column by column.
