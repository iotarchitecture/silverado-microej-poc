# Description
Display stack implementation with the following configuration:
  * BPP = 4: A pixel is represented on 4 bits maximum.
  * Layout = column: column by column.
