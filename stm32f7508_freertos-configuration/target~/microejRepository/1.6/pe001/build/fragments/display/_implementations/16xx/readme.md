# Description
Display stack implementation with the following configuration:
  * BPP = 16: A pixel is represented on 16 bits maximum.
  * Layout = line: line by line.
