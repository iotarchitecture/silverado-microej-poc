# Description
Display stack implementation with the following configuration:
  * BPP = 1: A pixel is represented on 1 bit maximum.
  * Layout = column: column by column.
