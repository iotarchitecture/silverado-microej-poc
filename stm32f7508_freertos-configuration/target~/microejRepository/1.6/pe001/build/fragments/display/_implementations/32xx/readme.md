# Description
Display stack implementation with the following configuration:
  * BPP = 32: A pixel is represented on 32 bits maximum.
  * Layout = line: line by line.
