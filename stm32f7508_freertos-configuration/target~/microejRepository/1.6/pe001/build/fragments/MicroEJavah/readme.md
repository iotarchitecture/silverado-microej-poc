
<!--
	Markdown
	
	Copyright 2014-2015 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->
				
# Description
Include a tool to generate a C header file (.h) from a Java class with natives methods. 
This tool is then available in the Run configuration > MicroEJ Tool > Execution settings: MicroEJavaH
