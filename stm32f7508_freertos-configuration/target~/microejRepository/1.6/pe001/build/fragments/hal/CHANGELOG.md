<!--
	Markdown
	Copyright 2016 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

## 1.0.4 (2016-04-07)
Features:
  - Initial public revision.
