<!--
	Markdown
	Copyright 2015-2016 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

# Overview
This library contains classes and interfaces stubs of Hardware Abstraction Layer API (ESR028).

# Usage
Add the following line to your `module.ivy` or your `ivy.xml`:
> `<dependency org="ej.api" name="hal" rev="1.+"/>`

# Requirements
  - EDC-1.2 or higher

# Dependencies
None.

# Source
N/A

# Restrictions
None.