<!--
	Markdown
	
	Copyright 2014-2018 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

## 2.0.1  (2018-04-13)

Bugfixes:

  - WI 20744: Net pack scripts may reference a directory that doesn't exist
  
## 2.0.0  (2018-01-22)

Features:

  - WI 20454: Net Library and Pack review, optimization and refactoring
  
## 1.0.5  (2017-03-14)

Bugfixes:

  - WI 19356: Wrong Labels on Net Embedded networksettings Workbench Extension

## 1.0.4  (2017-03-06)

Bugfixes:

  - WI 19242: Readme + changelog not embed in "net-embedded-networksettings-wbe" rip

## 1.0.3  (2017-03-03)

Bugfixes:

  - WI 17014: Missing description for Net and SSL in MicroEJ SDK 4 .platform -> content view -> details -> description

## 1.0.2  (2016-01-17)

Bugfixes:

  - WI 17125: IPv4 address checks are too restrictive

## 1.0.1  (2015-12-07)

Bugfixes:

  - WI 14756: XPFP Net: LLNET_Cfg.h is missing

## 1.0.0  (2015-07-16)

Features:

  - Initial revision.

