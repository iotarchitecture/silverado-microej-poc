<!--
	Markdown
	
	Copyright 2014-2020 MicroEJ Corp. All rights reserved.
	This library is provided in source code for use, modification and test, subject to license terms.
	Any modification of the source code will break MicroEJ Corp. warranties on the whole library.
-->

# Description
Add MicroUI user interface library.

# Configuration
  * Requires "microui/microui.xml" file
  
# References
  * Embedded UI extension reference manual