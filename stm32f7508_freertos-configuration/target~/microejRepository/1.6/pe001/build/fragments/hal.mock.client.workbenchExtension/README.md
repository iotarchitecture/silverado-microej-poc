<!--
	Markdown
	
	Copyright 2017 IS2T. All rights reserved.
	Modification and distribution is permitted under certain conditions.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

# Description
Add HAL configuration functionalities.

# Dependencies
  * HAL Mock