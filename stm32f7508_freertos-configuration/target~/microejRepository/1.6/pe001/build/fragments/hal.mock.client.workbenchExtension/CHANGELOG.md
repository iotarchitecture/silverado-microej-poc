<!--
	Markdown
	
	Copyright 2017 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

## 1.0.2  (2017-03-08)

Bugfixes:

  - Missing description for HAL in MicroEJ SDK 4 .platform -> content view -> details -> description

## 1.0.1  (2016-02-11)

Bugfixes:

  - Fix mock mode selection

## 1.0.0  (2016-02-05)

Features:

  - Initial revision.

