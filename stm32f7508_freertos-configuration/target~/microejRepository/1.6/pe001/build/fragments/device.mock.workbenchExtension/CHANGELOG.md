<!--
	Markdown
	Copyright 2017 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->
<!--
Changelog template:

## Revision (YYYY-mm-dd)
Features:
  - List here the new features.
  
Bugfixes:
  - List here the bug fixes.
-->

## 1.3.1 (2017-02-28)
Bugfixes:
  - Set platform module name.
  
## 1.3.0 (2017-02-01)
Features:
  - Implements FrontPanel extension to customize the FrontPanel window title.
  - Allow to specify the length of the ID using s3.mock.device.id.length property.

## 1.2.1 (2017-01-27)
Bugfixes:
  - Fix issue in scripts that prevents execution on embedded platform.

## 1.2.0 (2017-01-25)
Features:
  - Add autoConfiguration script for the XPF to defined the default device architecture in the mock.

## 1.1.0 (2017-01-25)
Features:
  - Review format of the property for the device id.

## 1.0.0 (2017-01-23)
Features:
  - Initial revision.

