<!--
	Markdown
	Copyright 2014-2017 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

# Overview
This library contains interfaces for getting device information.

# Usage
Add the following line to your `module.ivy` or your `ivy.xml`:
> `<dependency org="ej.api" name="device" rev="1.+"/>`

# Requirements
  - EDC-1.2 or higher
 
# Dependencies
None.

# Source
N/A

# Restrictions
None.