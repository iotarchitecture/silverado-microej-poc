<!--
	Markdown
	Copyright 2014-2017 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

## 1.0.2 (2017-02-28)
Bugfixes:
  - Fix README.
  - Set platform module name.
  
## 1.0.1 (2016-05-23)
Features:
  - Fix javadoc.

## 1.0.0 (2015-10-30)
Features:
  - Initial public revision.
