<!--
	Markdown
	
	Copyright 2015 IS2T. All rights reserved.
	Modification and distribution is permitted under certain conditions.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->
# ResourceManager-1.0
MicroEJ Java library implementation for runtime resource management.