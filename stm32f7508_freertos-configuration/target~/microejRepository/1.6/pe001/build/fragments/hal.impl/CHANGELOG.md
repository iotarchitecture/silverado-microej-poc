<!--
	Markdown
	
	Copyright 2015-2017 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

## 1.0.4  (2017-06-01)

Bugfixes:

  - WI19365: No Java lib version in CCO ivy module name.
  
## 1.0.3  (2017-03-08)
  
Bugfixes:

  - WI 19270: Missing C header files metadata in HAL low level headers artifact.
  - WI 17014: Missing description for Net and SSL in MicroEJ SDK 4 .platform -> content view -> details -> description.
  
## 1.0.2  (2016-02-11)
  
Bugfixes:

  - WI 15396: Extract HAL API from impl (without implementation code).

## 1.0.1  (2016-02-05)
  
Features:

  - Initial revision.
