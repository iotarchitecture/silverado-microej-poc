<!--
	Markdown
	
	Copyright 2014 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

# Description
Update associated BSP C project (<jpf>-bsp) with JPF libraries, include paths 
and optional C drivers.

# Configuration
* Requires "bsp/bsp.properties" file