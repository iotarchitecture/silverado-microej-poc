<!--
	Markdown
	
	Copyright 2015-2018 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

## 1.0.0 (2018-01-22)

Features:

  - Initial revision.

