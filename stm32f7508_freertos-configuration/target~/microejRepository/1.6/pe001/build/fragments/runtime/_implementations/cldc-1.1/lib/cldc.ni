<!--
	Natives Interface
	
	Copyright 2014 IS2T. All rights reserved.
	Modification and distribution is permitted under certain conditions.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->
<nativesInterface>

	<nativesPool name="ist.mowana.vm.GenericNativesPool">
		<native name="java.lang.ref.Reference.enqueue(java.lang.ref.Reference)boolean"/>
		<native name="java.lang.ref.ReferenceQueue.poll(java.lang.ref.ReferenceQueue)java.lang.ref.Reference"/>
		<native name="java.lang.ref.ReferenceQueue.waitForReference(long)void"/>
	</nativesPool>
	
</nativesInterface>