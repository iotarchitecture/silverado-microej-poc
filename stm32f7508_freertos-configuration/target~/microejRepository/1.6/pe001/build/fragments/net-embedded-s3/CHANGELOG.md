<!--
	Markdown
	
	Copyright 2014-2018 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

## 5.1.1 (2018-07-27)

Bugfixes:

  - M0172MEJANET-96 LLNET API: port format in LLNET_DATAGRAMSOCKETCHANNEL_IMPL_receive not specified
  
## 5.1.0 (2018-01-22)

Features:

  - WI 20454: Net Library and Pack review, optimization and refactoring
  
## 5.0.0 (2017-11-06)

Features:

  - WI 20421: Net: Remove dispatch from the pack
  
## 4.3.3 (2017-03-31)

Bugfixes:

  - WI 19490: Virtual Devices are slow to enumerate network interfaces

## 4.3.2 (2017-03-06)

Bugfixes:

  - WI 19243: Readme + changelog not embed in "net-embedded-s3" rip

## 4.3.1 (2017-03-03)

Bugfixes:

  - WI 17014: Missing description for Net and SSL in MicroEJ SDK 4 .platform -> content view -> details -> description

## 4.3.0 (2016-12-27)

Features:

  - Update dependencies to net-embedded-errors and net-embedded-dns-native.

## 4.2.2 (2016-12-26)

Bugfixes:

  - WI 18527: missing changelog info

## 4.2.1 (2016-12-14)

Bugfixes:

  - WI 18458: IPv6 addresses are used if available

## 4.2.0 (2016-11-16)

Features:

  - WI 11885: NET 1.1
  - WI 12273: Net Embedded Multicast impl + S3
  
Bugfixes:

  - WI 17062: Network Interface name not correctly decoded to the Mock
  - WI 17138: PortUnreachableException is never thrown

## 4.1.0 (2016-03-22)

Features:

  - WI 15714: DNS client on DatagramSocketImpl refactor

## 4.0.3 (2016-03-17)

Features:

  - WI 15216: Update Net native method to match right arguments when buffer exchanged

## 4.0.2 (2016-03-17)

Features:

  - WI 15216: Update Net native method to match right arguments when buffer exchanged

## 4.0.1 (2016-03-17)

Features:

  - WI 15686: Add network not initialized specific error code
  
## 4.0.0 (2016-03-01)

Features:

  - WI 15340: Add DNS Java support in XPFP Net
  
## 3.0.0 (2016-01-29)

Features:

  - WI 12279: Pull-up TCP/IP stack error codes
  
## 2.0.0 (2015-07-24)

Features:

  - WI 12379: Net-Embedded Impl on OpenJDK
  - WI 13345: rename NET-EMBEDDED into NET
  
## 1.0.2 (2015-04-15)

Bugfixes:

  - WI 12519 : [RT #1185] Socket.connect() doesn't throw any SocketTimeoutException
  
## 1.0.1 (2015-02-24)
  
Bugfixes:

  - WI 12207: DatagramPacket.getLength() method always return 4
  
## 1.0.0  (2014-xx-xx)

Features:

  - Initial revision.

