# Overview

This pack provides the `net`, `ssl` and `security` foundation libraries.

# Usage

Add the following line to your `module.ivy`:

	@MMM_DEPENDENCY_DECLARATION@

# Requirements

This library requires the following Foundation Libraries:

	@FOUNDATION_LIBRARIES_LIST@

# Dependencies

_All dependencies are retrieved transitively by MicroEJ Module Manager_.

# Source

N/A

# Restrictions

None.

---
_Copyright 2015-2020 MicroEJ Corp. All rights reserved._  
_MicroEJ Corp. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms._