<!--
	Markdown
	
	Copyright 2019 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->
# Description
Front Panel modules group. The Front Panel allows to display a splash screen and add some user interactions with the MicroEJ application.

# Note
This group is not compatible with UI pack < 12.0.0: this pack contains too a Front Panel module which conflicts with this one. 

# Migration Guide
This chapter explains how to migrate a front panel project compatible with UI pack < 12.0.0. This migration guide considers an UI pack 12.0.0 or higher is installed.

## Create new fp project

1. Verify that FrontPanelDesigner is at least version 6 : `Help > About > Installations Details > Plug-ins`.
1. Create a new front panel project: `File > New > Project... > MicroEJ > MicroEJ Front Panel Project`, choose a name and press `Finish`.
1. Move files from `[old project]/src` to `[new project]/src/main/java`.
1. Move files from `[old project]/resources` to `[new project]/src/main/resources`.
1. Move files from `[old project]/definitions` to `[new project]/src/main/resources`, **except** your `xxx.fp` file.
1. If existing delete file `[new project]/src/main/java/microui.properties`.
1. Delete file `[new project]/src/main/resources/.fp.xsd`.
1. Delete file `[new project]/src/main/resources/.fp1.0.xsd`.
1. Delete file `[new project]/src/main/resources/widgets.desc`.
1. Open `[old project]/definitions/xxx.fp`.
1. Copy `device` attributes (`name` and `skin`) from `[old project]/definitions/xxx.fp` to `[new project]/src/main/resources/xxx.fp`.
1. Copy content of `body` (not `body` tag itself) from `[old project]/definitions/xxx.fp` under `device` group of  `[new project]/src/main/resources/xxx.fp`.

Next chapters explain how to update each widget.

## Widget "led2states"

1. Rename `led2states` by `ej.fp.widget.LED`.
1. Rename the attribute `id` by `label`.

## Widget "pixelatedDisplay"

1. Rename `pixelatedDisplay` by `ej.fp.widget.Display`.
1. Remove the attribute `id`.
1. (_if set_) Remove the attribute `initialColor` if its value is `0`
1. (_if set_) Rename the attribute `mask` by `filter`; this image must have the same size in pixels than display itself (`width` * `height`).
1. (_if set_) Rename the attribute `realWidth` by `displayWidth`.
1. (_if set_) Rename the attribute `realHeight` by `displayHeight`.
1. (_if set_) Rename the attribute `transparencyLevel` by `alpha`; change the value: `newValue = 255 - oldValue` .
1. (_if set_) Remove the attribute `residualFactor` (not supported).
1. (_if set_) If `extensionClass` is specified: follow next notes.

**ej.fp.widget.Display Extension Class**  

1. Open the class
1. Extends `ej.fp.widget.MicroUIDisplay.AbstractDisplayExtension` instead of `com.is2t.microej.frontpanel.display.DisplayExtension`.
1. Rename method `convertDisplayColorToRGBColor` to `convertDisplayColorToARGBColor`.
1. Rename method `convertRGBColorToDisplayColor` to `convertARGBColorToDisplayColor`.

## Widget "pointer"

1. Rename `pointer` by `ej.fp.widget.Pointer`.
1. Remove the attribute `id`.
1. (_if set_) Rename the attribute `realWidth` by `areaWidth`.
1. (_if set_) Rename the attribute `realHeight` by `areaHeight`.
1. Keep or remove the attribute `listenerClass` according next notes.

**ej.fp.widget.Pointer Listener Class**  
  
This extension class is useless if the implementation respects these rules:
* _(a)_ `press` method is sending a `press` MicroUI Pointer event.
* _(b)_ `release` method is sending a `release` MicroUI Pointer event.
* _(c)_ `move` method is sending a `move` MicroUI Pointer event.
* _(d)_ The MicroUI Pointer event generator name is `POINTER` when `ej.fp.widget.Pointer`'s `touch` attribute is `false` (or not set).
* _(e)_ The MicroUI Pointer event generator name is `TOUCH` when `ej.fp.widget.Pointer`'s `touch` attribute is `true`.

If only _(d)_ or _(e)_ is different: 
1. Open the listener class.
1. Extends the class `ej.fp.widget.Pointer.PointerListenerToPointerEvents` instead of implementing the interface .`com.is2t.microej.frontpanel.input.listener.PointerListener`
1. Implements the method `getMicroUIGeneratorTag()`.

In all other cases:
1. Open the listener class.
1. Implements the interface `ej.fp.widget.Pointer.PointerListener` instead of `com.is2t.microej.frontpanel.input.listener.PointerListener`.

## Widget "push"

1. Rename `push` by `ej.fp.widget.Button`.
1. Rename the attribute `id` by `label`.
1. (_if set_) Review `filter` image: this image must have the same size in pixels than the button `skin`.
1. (_if set_) Remove the attribute `hotkey` (not supported).
1. Keep or remove the attribute `listenerClass` according next notes.

**ej.fp.widget.Button Listener Class**  

This extension class is useless if the implementation respects these rules:
* _(a)_ `press` method is sending a `press` MicroUI Buttons event with button `label` (equals to old button `id`) as button index.
* _(b)_ `release` method is sending a `release` MicroUI Buttons event with button `label` (equals to old button `id`) as button index.
* _(c)_ The MicroUI Buttons event generator name is `BUTTONS`.

If only _(c)_ is different: 
1. Open the listener class.
1. Extends the class `ej.fp.widget.Button.ButtonListenerToButtonEvents` instead of implementing the interface `com.is2t.microej.frontpanel.input.listener.ButtonListener`.
1. Overrides the method `getMicroUIGeneratorTag()`.

In all other cases:
1. Open the listener class.
1. Implements the interface `ej.fp.widget.Button.ButtonListener` instead of `com.is2t.microej.frontpanel.input.listener.ButtonListener`.

## Widget "repeatPush"
1. Rename `repeatPush` by `ej.fp.widget.RepeatButton`.
1. (_if set_) Remove the attribute `sendPressRelease` (not supported).
1. Same rules than widget _push_.

## Widget "longPush"
1. Rename `longPush` by `ej.fp.widget.LongButton`.
1. Same rules than widget _push_.

## Widget "joystick"
1. Rename `joystick` by `ej.fp.widget.Joystick`.
1. Remove the attribute `id`.
1. (_if set_) Rename the attribute `mask` by `filter`; this image must have the same size in pixels than joystick `skin`.
1. (_if set_) Remove the attribute `hotkeys` (not supported).
1. Keep or remove the attribute `listenerClass` according next notes.

**ej.fp.widget.Joystick Listener Class**  

This extension class is useless if the implementation respects these rules:
* _(a)_ `press` methods are sending some MicroUI Command events `UP`, `DOWN`, `LEFT`, `RIGHT` and `SELECT`.
* _(b)_ `repeat` methods are sending same MicroUI Command events `UP`, `DOWN`, `LEFT`, `RIGHT` and `SELECT`.
* _(c)_ `release` methods are sending nothing.
* _(d)_ The MicroUI Command event generator name is `JOYSTICK`.

If only _(d)_ is different: 
1. Open the listener class
1. Extends the class `ej.fp.widget.Joystick.JoystickListenerToCommandEvents` instead of implementing the interface `com.is2t.microej.frontpanel.input.listener.JoystickListener`.
1. Overrides the method `getMicroUIGeneratorTag()`.

In all other cases:
1. Open the listener class.
1. Implements the interface `ej.fp.widget.Joystick.JoystickListener` instead of `com.is2t.microej.frontpanel.input.listener.JoystickListener`.

## Others Widgets
These widgets may have not been migrated. Check in `ej.tool.frontpanel.widget` library if some widgets are compatible or write your own widgets.
