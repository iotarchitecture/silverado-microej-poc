<!--
	Markdown
	
	Copyright 2017 IS2T. All rights reserved.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->

# Description
Device library modules group. This API provides access to the device information (architecture name and device identifier).