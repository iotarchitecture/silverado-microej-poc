<?xml version="1.0" encoding="UTF-8"?>
<!--
	ANT
 
	Copyright 2011-2021 MicroEJ Corp. All rights reserved.
	This library is provided in source code for use, modification and test, subject to license terms.
	Any modification of the source code will break MicroEJ Corp. warranties on the whole library.
-->
<project name="microuiInit">

	<dirname file="${ant.file.microuiInit}" property="microuiInit.dir"/>
	<import file="${microuiInit.dir}/../extension-init.xml" />
	
	<extension-point name="init/microui/context/hook" description="Hook to force to use MicroUI (see MWT)" />
	
	<target name="init/microui/check" extensionOf="init/context" depends="init/microui/context/hook">
		<!-- check if application targets MicroUI -->
		<available classpath="${application.classpath}" classname="ej.microui.MicroUI" property="usesMICROUI"/>
	</target>
	
	<!-- this target is always executed even if we are not using microui -->
	<target name="init/microui/options" extensionOf="init/execution">
		
		<!-- retrieve some optional platform properties -->
		<property file="${microuiInit.dir}/microui.properties" prefix="microui."/>
		
		<!-- set default properties values (in case of we are not using MicroUI) -->
		<condition property="ej.microui.memory.imagesheap.size" value="${microui.imagesHeap.size}" else="128*1024"  description="default images heap size (same in workbench extension)">
			<isset property="microui.imagesHeap.size"/>
		</condition>
	</target>
	
	<!-- third party tools should be executed before initializing microui properties/constants/resources -->
	<extension-point name="init/microui/thirdparty"/>
	
	<target name="init/microui" extensionOf="init/execution" depends="init/microui/options, init/microui/thirdparty" if="usesMICROUI">
		
		<echo message="MicroUI init" level="verbose" />
		
		<!-- CLASSPATH -->
		<augment id="init.application.classpath">
			<fileset dir="${jpf.dir}/javaLibs" includes="microui-*.jar" />
			<fileset dir="${jpf.dir}/javaLibs" includes="systemmicroui-*.jar" />
			<fileset dir="${jpf.dir}/javaLibs" includes="trace-*.jar" />
		</augment>
		
		<!-- REQUIRES (system microui class)-->
		<property file="${jpf.dir}/javaLibs/systemmicroui.properties"/>
		<fail unless="microui.require" description="platform specific, must be set in systemmicroui.properties"/>
		<augment id="init.requires">
			<string value="${microui.require}"/>
		</augment>		
		<property name="com.microej.library.microui.impl.class" value="${microui.require}"/>
		
		<!-- REQUIRES (generic event generator) -->
		<condition property="microui.eventgen.require" value="${microui.eventgen}" else="ej.microui.MicroUI" description="fake value in case of there is no generic event generator.">
			<and>
				<isset property="microui.eventgen"/>
				<not><equals arg1="${microui.eventgen}" arg2=""/></not>
			</and>
		</condition>
		<script language="javascript"> 
			list = project.getProperty("microui.eventgen.require").split(project.getProperty("path.separator"));
			var availableTask = self.project.createTask("available");
			availableTask.setClasspath(project.getReference("init.application.classpath"));
			for(var i = 0; i &lt; list.length; i++) {
				project.setProperty("microui.eventgen", list[i]);
				availableTask.setClassname(list[i]);
				availableTask.setProperty("microui.eventgen."+list[i]+".available");
				availableTask.perform();
				if (project.getProperty("microui.eventgen."+list[i]+".available") == "true"){
					self.project.createTask("init/microui/genericEventGenerator/require").execute();
				}
			}
		</script>
		
		<!-- RESOURCES -->
		<property name="microui.resources" value="" description="default value: no specific resource"/>	
		<augment id="init.resources">
			<string value="${microui.resources}"/>
		</augment>
		
		<!-- CONSTANTS -->
		<property name="com.microej.library.microui.assert.enabled" value="false" description="no assert by default"/>
		<condition property="com.microej.library.microui.images.logs.enabled" value="true" else="false" description="'com.is2t.microui.log' is old property (backward compatible)">
			<istrue value="${com.is2t.microui.log}"/>
		</condition>
		<condition property="com.microej.library.microui.onS3" value="true" else="false">
			<isset property="onS3"/>
		</condition>
		<property name="com.microej.library.microui.pump.watchdog.delay" value="5000" description="default watchdog timeout"/>
		<property name="com.microej.library.microui.pump.priority" value="5" description="default ui pump priority"/>
		<property name="ej.microui.fonts.list" value="" description="default value: no font"/>
		<property name="com.microej.library.microui.impl.edc-internal" value="true" description="can use edc internal"/>
		<augment id="init.constants">
			<propertyref name="com.microej.library.microui.impl.class"/>
			<propertyref name="com.microej.library.microui.impl.edc-internal"/>
			<propertyref name="com.microej.library.microui.assert.enabled"/>
			<propertyref name="com.microej.library.microui.images.logs.enabled"/>
			<propertyref name="com.microej.library.microui.pump.watchdog.delay"/>
			<propertyref name="com.microej.library.microui.pump.priority"/>
			<propertyref name="com.microej.library.microui.onS3"/>
			<propertyref name="ej.microui.fonts.list"/>
		</augment>
		
	</target>
	
	<!-- force to embed the generic event generator -->
	<macrodef name="init/microui/genericEventGenerator/require">
		<sequential>
			<augment id="init.requires">
				<string value="${microui.eventgen}"/>
			</augment>
		</sequential>
	</macrodef>
	
</project>