<!--
	Natives Interface
	
	Copyright 2019-2020 IS2T. All rights reserved.
	Modification and distribution is permitted under certain conditions.
	IS2T PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
-->
<nativesInterface>
	
	<nativesPool name="ist.microjvm.NativesPool">
		<native name="ej.sni.NativeResource.getRegisteredNativeResourcesCount()int"/>
		<native name="ej.sni.NativeResource.getDescription(ej.sni.NativeResource,byte[])void"/>
		<native name="ej.sni.NativeResource.getNativeResource(long,long)ej.sni.NativeResource"/>
		<native name="ej.sni.NativeResource.getOwner()java.lang.Object"/>
	</nativesPool>
	
</nativesInterface>