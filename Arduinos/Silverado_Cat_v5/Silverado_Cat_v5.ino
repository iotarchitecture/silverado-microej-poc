/*********************************************************************

*********************************************************************/

#include <bluefruit.h>
#include <SoftwareSerial.h>



BLEService         bleSrvcMewo ("0000ffe0-0000-1000-8000-00805F9B34FB");
BLECharacteristic  bleCharPos  ("0000ffe1-0000-1000-8000-00805F9B34FB");
BLECharacteristic  bleCharCmd  ("0000ffe2-0000-1000-8000-00805F9B34FB");

uint8_t m_char_command[16] = { 0,0,0,0,  0,0,0,0,      0,0,0,0,  0,0,0,0 };

uint8_t m_char_position[8] = { '0', 0,   0,0,   0,0,0,0 };

int     m_mewo_position0 = (0);
int     m_mewo_position  = (0);
int     m_mewo_offset    = (0);
int     m_mewo_incr      = (0);
int     m_mewo_target    = (0);
int     m_mewo_report    = (0);


void setup()
{
    Serial.begin(115200);
    while (! Serial)  {
        delay(10);   // for nrf52840 with native usb
    }
  
    Serial.print("PSEUDO-AutoStop  (-Bluefruit52-)\r\n");
    Serial.print("----------------------------------\r\n\r\n");


    // Initialise the Bluefruit module
    Serial.println("Initialize the Bluefruit nRF52 module");
    //// Peripheral connections: 1-max, Central connections: 0-max
    Bluefruit.begin(1, 0);

    //// setup power level 4, using type-0A ADV-data
    Bluefruit.setTxPower(4);

    //// set the name using type-09 ADV-data
    Bluefruit.setName("AutoStop");

    // Set the connect/disconnect callback handlers
    Bluefruit.Periph.setConnectCallback(connect_callback);
    Bluefruit.Periph.setDisconnectCallback(disconnect_callback);

    // Setup the *M*E*W*O* service using BLEService and BLECharacteristic classes
    Serial.println("Configuring the Mewo Service");
    initiateService_Mewo();
  
    // Setup the advertising packet(s)
    Serial.println("Setting up the advertising payload(s)");
    startAdvertisements();
  
    Serial.print("\r\nStart Advertisments\r\n");

    //// turn off the LED
    digitalWrite(LED_RED, 0);
}




void startAdvertisements(void)
{
    // Advertising packet
    Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);

    //// include TX power indication
    Bluefruit.Advertising.addTxPower();

    //// Include Name
    Bluefruit.Advertising.addName();
    
    // Start Advertising
    // - Enable auto advertising if disconnected
    // - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
    // - Timeout for fast mode is 30 seconds
    // - Start(timeout) with timeout = 0 will advertise forever (until connected)
    // 
    // For recommended advertising interval
    // https://developer.apple.com/library/content/qa/qa1931/_index.html   
    //
    Bluefruit.Advertising.restartOnDisconnect(true);
    Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
    Bluefruit.Advertising.setFastTimeout(60);      // number of seconds in fast mode
    Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds  
}




void initiateService_Mewo()
{
    //// start the SERVICE object
    bleSrvcMewo.begin();


    //// set properties for the CHARACTERISTIC *Mewo*Position*
    bleCharPos.setProperties(CHR_PROPS_NOTIFY | CHR_PROPS_WRITE);
    bleCharPos.setPermission(SECMODE_OPEN, SECMODE_NO_ACCESS);
    bleCharPos.setFixedLen(8);
    bleCharPos.setCccdWriteCallback(cccd_cb_position);
    bleCharPos.begin();

    //// set initial value for CHARACTERISTIC *Mewo*Position*
    bleCharPos.write(m_char_position, sizeof(m_char_position));


    //// set properties for the CHARACTERISTIC *Mewo*Command*
    bleCharCmd.setProperties(CHR_PROPS_READ | CHR_PROPS_WRITE);
    bleCharCmd.setPermission(SECMODE_OPEN,    SECMODE_OPEN);
    bleCharCmd.setFixedLen(16);
    bleCharCmd.setWriteCallback( bleCharCmd_write_callback );
    bleCharCmd.begin();

    //// set initial value for CHARACTERISTIC *Mewo*Command*
    bleCharCmd.write(m_char_command, sizeof(m_char_command));
}




void connect_callback(uint16_t conn_handle)
{
  // Get the reference to current connection
  BLEConnection* connection = Bluefruit.Connection(conn_handle);

  char central_name[32] = { 0 };
  connection->getPeerName(central_name, sizeof(central_name));

  Serial.print("BLE Connection established ~ with ");
  Serial.println(central_name);
}


/**
 * Callback invoked when a connection is dropped
 * @param conn_handle connection where this event happens
 * @param reason is a BLE_HCI_STATUS_CODE which can be found in ble_hci.h
 */
void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
    (void) conn_handle;
    (void) reason;

    Serial.print("BLE Disconnected ~ reason = 0x"); Serial.print(reason, HEX);

    Serial.print("\r\nStart Advertisments\r\n");
}




void cccd_cb_position(uint16_t conn_hdl, BLECharacteristic* chr, uint16_t cccd_value)
{
    // Display the raw request packet
    Serial.print("CCCD Updated: ");
    //Serial.printBuffer(request->data, request->len);
    Serial.print(cccd_value);
    Serial.println("");

    // Ensure this CCCD-Update is associated with the POSITION-Characteristic
    if (chr->uuid == bleCharPos.uuid) {
        Serial.println("Char-Position CCCD handler");
        if (chr->notifyEnabled(conn_hdl)) {
            Serial.println("Char-Position 'Notify' enabled");
        } else {
            Serial.println("Char-Position 'Notify' disabled");
        }
    }
}










int instr(char *s, char *m)
{
    int i,j,k;

    if (NULL==s || NULL==m) {
        return (-1);
    }
    if (('\0')==*s || ('\0')==*m) {
        return (-1);
    }

    for (i=0; (s[i]); ++i) {
        for (k = (0), j = i; (1); ++j) {
            char ch = m[k++];
            if (('\0')==ch) {
                return (i);
            }
            if (ch != s[j]) {
                break;
            }
        }
    }

    return (-1);
}











#define CMD_CALIBRATE_LEFT    ("stl:")
#define CMD_CALIBRATE_RIGHT   ("str:")
#define CMD_CALIBRATE_OFFSET  ("cal ")
#define CMD_GOTO_POSITION     ("gtp ")

#define CMD_MANUAL_POSITION   ("hnd:")
#define CMD_MANUAL_BRAKE      ("brk:")
#define CMD_STOP              ("stp:")


void  bleCharCmd_write_callback(uint16_t conn_hdl, BLECharacteristic* chr, uint8_t *pData, uint16_t len)
{
    (void) conn_hdl;
    (void) chr;
    (void) len;
    int  param;

    if (0 == instr( (char *)pData,  CMD_CALIBRATE_LEFT )) {
        command_calibrate_left();
        return;
    }
    if (0 == instr( (char *)pData,  CMD_CALIBRATE_RIGHT )) {
        command_calibrate_right();
        return;
    }
    if (0 == instr( (char *)pData,  CMD_CALIBRATE_OFFSET )) {
        param = atoi((char *)(pData + 4));
        command_calibrate_offset(param);
        return;
    }
    if (0 == instr( (char *)pData,  CMD_GOTO_POSITION )) {
        param = atoi((char *)(pData + 4));
        command_goto_position(param);
        return;
    }
    if (0 == instr( (char *)pData,  CMD_MANUAL_POSITION )) {
        command_manual_position();
        return;
    }
    if (0 == instr( (char *)pData,  CMD_MANUAL_BRAKE )) {
        command_manual_brake();
        return;
    }
    if (0 == instr( (char *)pData,  CMD_STOP )) {
        command_stop();
        return;
    }


    Serial.print("----UNrecognized-Command---{");
    Serial.print( (char *)pData );
    Serial.print("}---\r\n");
}


char report_str[40];

void command_calibrate_left()
{
    report ("CALIBRATE-LEFT");
    m_mewo_target = (2000);
    m_mewo_incr   = (1);
}


void command_calibrate_right()
{
    report ("CALIBRATE-RIGHT");
    m_mewo_target = (0);
    m_mewo_incr   = (-1);
}


void command_calibrate_offset(int n)
{
    sprintf (report_str, "CALIBRATE-OFFSET(%d)", n);
    report (report_str);
}


void command_goto_position(int n)
{
    sprintf (report_str, "GOTO(%d)", n);
    m_mewo_target = (n);
    if (n < m_mewo_position)
        m_mewo_incr = (-1);
    else
        m_mewo_incr = (1);

    m_mewo_position0 = (-1);
    report (report_str);
}


void command_manual_position()
{
    report ("MANUAL-MODE");

    m_mewo_position0 = (-1);
    m_mewo_incr      = (0);
}


void command_manual_brake()
{
    report ("BRAKE");

    m_mewo_position0 = (-1);
    m_mewo_incr      = (0);
}


void command_stop()
{
    report ("STOP");

    m_mewo_position0 = (-1);
    m_mewo_target = (m_mewo_position);
    m_mewo_incr   = (0);
}


void report(char *s)
{
    Serial.print("==[ ");
    Serial.print(s);
    Serial.print(" ]===\r\n");
}





char rrr[80];

void loop()
{
    if (Bluefruit.connected())
    {
        sprintf(rrr, "target=%d  pos=%d  inc=%d", m_mewo_target, m_mewo_position, m_mewo_incr);
        Serial.println(rrr);

        m_mewo_position += m_mewo_incr;

        if (m_mewo_incr > 0) {
            if (m_mewo_position >= (m_mewo_target + m_mewo_offset)) {
                m_mewo_position = (m_mewo_target + m_mewo_offset);
                m_mewo_incr = (0);
            }
            else if (m_mewo_incr < 20) {
                ++m_mewo_incr;
            }
        }
        else if (m_mewo_incr < 0) {
            if (m_mewo_position <= (m_mewo_target + m_mewo_offset)) {
                m_mewo_position = (m_mewo_target + m_mewo_offset);
                m_mewo_incr = (0);
            }
            else if (m_mewo_incr > -20) {
                --m_mewo_incr;
            }
        }


        //////// report the position every loop, at once a second
        sprintf( (char *)m_char_position, "%d", m_mewo_position);
        ////
        if (! bleCharPos.notify(m_char_position, sizeof(m_char_position))) {
            ;
            //  Serial.print("***--Notify-not-set-");
        }
        //////// if the position is changing, only delay for 200-msec
        if (m_mewo_position != m_mewo_position0) {
            m_mewo_position0 = m_mewo_position;
            delay(200);
            return;
        }
    }

    // Only send update once per second
    delay(1000);
}
