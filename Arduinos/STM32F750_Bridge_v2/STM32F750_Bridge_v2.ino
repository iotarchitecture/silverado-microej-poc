/*********************************************************************

*********************************************************************/

#include <bluefruit.h>
#include <SoftwareSerial.h>



BLEClientService         bleSrvcMewo ("0000ffe0-0000-1000-8000-00805F9B34FB");
BLEClientCharacteristic  bleCharPos  ("0000ffe1-0000-1000-8000-00805F9B34FB");
BLEClientCharacteristic  bleCharCmd  ("0000ffe2-0000-1000-8000-00805F9B34FB");




void setup()
{
    Serial.begin(115200);
    while (! Serial)  {
        delay(10);   // for nrf52840 with native usb
    }

    Serial.print("# MBBRIDGE--BLE-to--AutoStop  (-Bluefruit52-)\r\n");
    Serial.print("# ----------------------------------------------\r\n\r\n");


    //// Initialise the Bluefruit module
    Serial.print("# Initialize the Bluefruit nRF52 module\r\n");
    //// Peripheral connections: 0-max, Central connections: 1-max
    Bluefruit.begin(0, 1);

    //// setup power level 4, using type-0A ADV-data
    Bluefruit.setTxPower(4);

    //// set the BLE device name
    Bluefruit.setName("MBBridge");



    //// initialize Service
    bleSrvcMewo.begin();

    //// initialize Characteristic Command
    bleCharCmd.begin();

    //// initialize Charateristic Position
    bleCharPos.setNotifyCallback(bleCharPos_callback_notify);
    bleCharPos.begin();



    //// Start Central Scan
    Bluefruit.setConnLedInterval(250);
    Bluefruit.Central.setDisconnectCallback(ble_callback_disconnect);
    Bluefruit.Central.setConnectCallback(ble_callback_connected);
    Bluefruit.Scanner.setRxCallback(ble_scan_callback);
    Bluefruit.Scanner.start(0);

    Serial.print("\r\n# Scanning\r\n");


    //// turn off the LED
    digitalWrite(LED_RED, 0);
}




#define       m_ble_DISCONNECTED             (0)
#define       m_ble_CONNECTED                (1)
#define       m_ble_CONNECTING               (2)

unsigned char m_ble_autostop_mac[6]          = {0,0,0, 0,0,0};
int           m_ble_autostop_rssi            = (-999);
int           m_ble_autostop_exptime         = (0);
int           m_ble_countDown_to_connect     = (0);
int           m_ble_countDown_to_connect_MAX = (32);
int           m_ble_connection               = (m_ble_DISCONNECTED);


void ble_scan_callback(ble_gap_evt_adv_report_t *report)
{
    unsigned char *pch = (unsigned char *)(report->data.p_data);
    int  len  = report->data.len;
    int  i0, i1, i2;


    i1 = uinstr(pch, len, (unsigned char *)("Auto"));
    i2 = uinstr(pch, len, (unsigned char *)("Stop"));
    if (len >= 16) {
        if ((i1>=2)
         && (i2 >= (i1 + 4))
         && (0x09==pch[i1-2]))  {
              //
              if ((0 != m_ble_autostop_exptime) && ((m_ble_autostop_exptime - 1000) <= millis())) {
                  m_ble_autostop_rssi = (-999);
                  m_ble_countDown_to_connect = (0);
                  Serial.print("# *\r\n");
              }
              //
              if (report->rssi >= m_ble_autostop_rssi)
              {
                  m_ble_autostop_rssi = (report->rssi);
                  //
                  m_ble_autostop_exptime = (millis() + (15 * 1000));
                  //
                  m_ble_autostop_mac[0] = (report->peer_addr.addr[5]);
                  m_ble_autostop_mac[1] = (report->peer_addr.addr[4]);
                  m_ble_autostop_mac[2] = (report->peer_addr.addr[3]);
                  m_ble_autostop_mac[3] = (report->peer_addr.addr[2]);
                  m_ble_autostop_mac[4] = (report->peer_addr.addr[1]);
                  m_ble_autostop_mac[5] = (report->peer_addr.addr[0]);
                  Serial.print("# *");
              }
              else {
                  Serial.print("#  ");
              }

              Serial.printf("%09d ", millis());
          
              //// MAC is in little endian --> print reverse
              Serial.printBufferReverse(report->peer_addr.addr, 6, ':');
              Serial.print("  ");
            
              Serial.print(report->rssi);
              Serial.print("    ");
            
              Serial.printBuffer(report->data.p_data, report->data.len, '-');
              Serial.print("\r\n");

              if ((0)==m_ble_countDown_to_connect) {
                  m_ble_countDown_to_connect = (m_ble_countDown_to_connect_MAX);
              }
              else {
                  -- m_ble_countDown_to_connect;
                  if ((0) == m_ble_countDown_to_connect)
                  {
                      if (m_ble_connection == m_ble_CONNECTING) {
                          Serial.print("# ****ALREADY-CONNECTING****\r\n");
                      }
                      else if (m_ble_connection == m_ble_CONNECTED) {
                          Serial.print("# ****ALREADY-CONNECTED****\r\n");
                      }
                      else {
                          m_ble_connection = m_ble_CONNECTING;
                          Serial.print("# ****CONNECT********\r\n");
                          Bluefruit.Central.connect(report);
                      }
                  }
              }
         }
    }

    if ((0 != m_ble_autostop_exptime) && (m_ble_autostop_exptime <= millis())) {
        Serial.print("# Autostop-Lost\n");
        m_ble_autostop_exptime = (0);
        m_ble_autostop_rssi = (-999);
        m_ble_countDown_to_connect = (0);
    }


    Bluefruit.Scanner.resume();
}



void connect_callback(uint16_t conn_handle)
{
  //// Get the reference to current connection
  BLEConnection* connection = Bluefruit.Connection(conn_handle);

  char central_name[32] = { 0 };
  connection->getPeerName(central_name, sizeof(central_name));

  Serial.print("# BLE Connection established ~ with ");
  Serial.print(central_name);
  Serial.print("\r\n");
}


/**
 * Callback invoked when a connection is dropped
 * @param conn_handle connection where this event happens
 * @param reason is a BLE_HCI_STATUS_CODE which can be found in ble_hci.h
 */
void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
    (void) conn_handle;
    (void) reason;

    Serial.print("# BLE Disconnected ~ reason = 0x"); Serial.print(reason, HEX);

    Serial.print("\r\n# Start Scanning\r\n");
}







int uinstr(unsigned char *s, int len, unsigned char *m)
{
    int i,j,k;

    if (NULL==s || NULL==m) {
        return (-1);
    }
    if (('\0')==*s || ('\0')==*m) {
        return (-1);
    }

    for (i=0; (i < len); ++i) {
        for (k = (0), j = i; (1); ++j) {
            char ch = m[k++];
            if (('\0')==ch) {
                return (i);
            }
            if (ch != s[j]) {
                break;
            }
        }
    }

    return (-1);
}



int instr(char *s, char *m)
{
    int i,j,k;

    if (NULL==s || NULL==m) {
        return (-1);
    }
    if (('\0')==*s || ('\0')==*m) {
        return (-1);
    }

    for (i=0; (s[i]); ++i) {
        for (k = (0), j = i; (1); ++j) {
            char ch = m[k++];
            if (('\0')==ch) {
                return (i);
            }
            if (ch != s[j]) {
                break;
            }
        }
    }

    return (-1);
}



int extractInteger( char *s )
{
    int  n;
    char ch, bFlagDigits;

    bFlagDigits = (0);
    n = (0);
    for (;;) {
        ch = *s++;
        if (('\0')==ch) {
            break;
        }
        if ((('0') <= ch) && (ch <= ('9'))) {
            bFlagDigits = (1);
            n *= (10);
            n += (int)(ch - ('0'));
        }
        else if (bFlagDigits) {
            break;
        }
    }
    return (n);
}





void ble_callback_disconnect(uint16_t conn_handle, uint8_t reason)
{
    (void) conn_handle;
    (void) reason;

    m_ble_connection = m_ble_DISCONNECTED;
    Serial.print("\r\n# Disconnected---  reason = 0x");
    Serial.print(reason, HEX);
    Serial.print("\r\n");
}




void ble_callback_connected(uint16_t conn_handle)
{
    m_ble_connection = m_ble_CONNECTED;
    Serial.print("\r\n# Connected----\r\n");

    Serial.print("# Discovering MEWO Service...");
    if (! bleSrvcMewo.discover(conn_handle)) {
        Serial.print("Found NONE!\r\n");
        Bluefruit.disconnect(conn_handle);
        return;
    }
    Serial.print("Found.\r\n");

    Serial.print("# Discovering MEWO Characteristic COMMAND...");
    if (! bleCharCmd.discover()) {
        Serial.print("Found NONE!\r\n");
        Bluefruit.disconnect(conn_handle);
        return;
    }
    Serial.print("Found.\r\n");

    Serial.print("# Discovering MEWO Characteristic POSITION...");
    if (! bleCharPos.discover()) {
        Serial.print("Found NONE!\r\n");
        Bluefruit.disconnect(conn_handle);
        return;
    }
    Serial.print("Found.\r\n");
    if (bleCharPos.enableNotify()) {
        Serial.print("# Ready to Receive Position Notificatinons\r\n");
    }
    else {
        Serial.print("# UNABLE to Receive Position Notificatinons\r\n");
    }
}



/**
 * Hooked callback that triggered when a measurement value is sent from peripheral
 * @param chr   Pointer client characteristic that even occurred,
 *              in this example it should be hrmc
 * @param data  Pointer to received data
 * @param len   Length of received data
 */
void bleCharPos_callback_notify(BLEClientCharacteristic* chr, uint8_t* data, uint16_t len)
{
    Serial.print("\r\n@ ");
    Serial.print((char *)data);
    Serial.print("\r\n");
}




#define UART_NOP               ("NOP")
#define UART_CALIBRATE_LEFT    ("RSTL")
#define UART_CALIBRATE_RIGHT   ("RSTR")
#define UART_CALIBRATE_OFFSET  ("CAL ")
#define UART_GOTO_POSITION     ("GTP ")
#define UART_MANUAL_POSITION   ("HND")
#define UART_MANUAL_BRAKE      ("BRK")
#define UART_STOP              ("STP")


#define CMD_NOP               ("nop: ")
#define CMD_CALIBRATE_LEFT    ("stl: ")
#define CMD_CALIBRATE_RIGHT   ("str: ")
#define CMD_CALIBRATE_OFFSET  ("cal ")
#define CMD_GOTO_POSITION     ("gtp ")

#define CMD_MANUAL_POSITION   ("hnd: ")
#define CMD_MANUAL_BRAKE      ("brk: ")
#define CMD_STOP              ("stp: ")


char  uart_cmd[32];
int   uart_cmd_len = (0);
int   uart_cmd_ready = (0);

unsigned char txBuff[20];


void tx_message(char *s)
{
    int n, i;

    if ((NULL != s) && (('\0') != *s)) {
        for (n=i=0; s[i]; ++i) {
            txBuff[n++] = (unsigned char)(s[i]);
            if (i >= 16) {
                break;
            }
        }
        bleCharCmd.write( txBuff, n );
    }
}



void relay_cmd(char *str)
{
    int n;

    //////// Error the command if BLE not connected
    if (m_ble_CONNECTED != m_ble_connection) {
        Serial.print("\r\nERROR - Not Connected\r\n");
        return;
    }

    if (-1 != instr(str, UART_NOP)) {
        Serial.print("\r\nOK - NOP\r\n");
        tx_message( CMD_NOP );
        return;
    }
    if (-1 != instr(str, UART_CALIBRATE_LEFT)) {
        Serial.print("\r\n\nOK - RSTL\r\n");
        tx_message( CMD_CALIBRATE_LEFT );
        return;
    }
    if (-1 != instr(str, UART_CALIBRATE_RIGHT)) {
        Serial.print("\r\nOK - RSTR\r\n");
        tx_message( CMD_CALIBRATE_RIGHT );
        return;
    }
    if (-1 != instr(str, UART_MANUAL_POSITION)) {
        Serial.print("\r\nOK - HND\r\n");
        tx_message( CMD_MANUAL_POSITION );
        return;
    }
    if (-1 != instr(str, UART_MANUAL_BRAKE)) {
        Serial.print("\r\nOK - BRK\r\n");
        tx_message( CMD_MANUAL_BRAKE );
        return;
    }
    if (-1 != instr(str, UART_STOP)) {
        Serial.print("\r\nOK - STP\r\n");
        tx_message( CMD_STOP );
        return;
    }


    if (-1 != instr(str, UART_CALIBRATE_OFFSET)) {
        n = extractInteger( str );
        Serial.print("\r\nOK - CAL ");
        Serial.print(n);
        Serial.print("\r\n");
        sprintf( (char *)txBuff, "%s%d: ",  CMD_CALIBRATE_OFFSET,  (n) );
        bleCharCmd.write( txBuff, strlen((char *)txBuff) );
        return;
    }

    if (-1 != instr(str, UART_GOTO_POSITION)) {
        n = extractInteger( str );
        Serial.print("\r\nOK - GTP ");
        Serial.print(n);
        Serial.print("\r\n");
        sprintf( (char *)txBuff, "%s%d: ",  CMD_GOTO_POSITION,  (n) );
        bleCharCmd.write( txBuff, strlen((char *)txBuff) );
        return;
    }

    Serial.print("\r\nERROR - \"");
    Serial.print(str);
    Serial.print("\"\r\n");
}






void loop()
{
    int  ch;

    //////// Assume no incoming characters
    ch = (-1);

    //////// look for incoming character from USB-SERIAL uart
    if( Serial.available() > 0) {
        ch = Serial.read();
    }

    //////// If character received, then consume it
    if (-1 != ch) {
        if (uart_cmd_len < (sizeof(uart_cmd) - 2)) {
            if (('\0')!=ch && ('\r')!=ch && ('\n')!=ch) {
                uart_cmd[ uart_cmd_len++ ] = (ch);
                uart_cmd[ uart_cmd_len ] = ('\0');
            }
            else {
                //// this flags the end of a command line received
                uart_cmd_ready = (1);
            }
        }
    }

    //////// If a complete command line received, consume it
    if (uart_cmd_ready)
    {
        //////// relay the command. (function will check if BLE connected)
        relay_cmd(uart_cmd);

        //////// clear for the next command
        uart_cmd_ready = (0);
        uart_cmd_len   = (0);
        uart_cmd[0]    = ('\0');
    }

}////-------END-of-LOOP-------










/*-
# MBBRIDGE--BLE-to--AutoStop  (-Bluefruit52-)
# ----------------------------------------------

# Initialize the Bluefruit nRF52 module

# Scanning
# *000000515 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000000543 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000000635 EB:C8:AF:E8:36:75  -72    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000000661 EB:C8:AF:E8:36:75  -73    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000000724 EB:C8:AF:E8:36:75  -65    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000000752 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000000825 EB:C8:AF:E8:36:75  -65    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000000853 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000000920 EB:C8:AF:E8:36:75  -75    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000000942 EB:C8:AF:E8:36:75  -76    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001018 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001045 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001125 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001146 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001221 EB:C8:AF:E8:36:75  -76    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001247 EB:C8:AF:E8:36:75  -77    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001327 EB:C8:AF:E8:36:75  -68    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001347 EB:C8:AF:E8:36:75  -69    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001424 EB:C8:AF:E8:36:75  -68    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001452 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001553 EB:C8:AF:E8:36:75  -76    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000001623 EB:C8:AF:E8:36:75  -65    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000001645 EB:C8:AF:E8:36:75  -65    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000001727 EB:C8:AF:E8:36:75  -65    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000001750 EB:C8:AF:E8:36:75  -65    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001822 EB:C8:AF:E8:36:75  -72    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000001852 EB:C8:AF:E8:36:75  -73    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000001926 EB:C8:AF:E8:36:75  -65    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000001947 EB:C8:AF:E8:36:75  -65    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000002028 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000002049 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000002126 EB:C8:AF:E8:36:75  -74    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000002154 EB:C8:AF:E8:36:75  -74    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# ****CONNECT********
*
# Connected----
# Discovering MEWO Service...Found NONE!

# Disconnected---  reason = 0x3E
#  000003183 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003199 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003223 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003303 EB:C8:AF:E8:36:75  -74    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003407 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003510 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003585 EB:C8:AF:E8:36:75  -74    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003608 EB:C8:AF:E8:36:75  -73    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003690 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003718 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003790 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003815 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003894 EB:C8:AF:E8:36:75  -74    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000003916 EB:C8:AF:E8:36:75  -74    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004005 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004104 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004183 EB:C8:AF:E8:36:75  -74    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004230 EB:C8:AF:E8:36:75  -74    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000004298 EB:C8:AF:E8:36:75  -65    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004322 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004400 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004424 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004499 EB:C8:AF:E8:36:75  -74    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004522 EB:C8:AF:E8:36:75  -75    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004588 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004609 EB:C8:AF:E8:36:75  -67    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004631 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004702 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004722 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004791 EB:C8:AF:E8:36:75  -74    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004814 EB:C8:AF:E8:36:75  -75    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
#  000004889 EB:C8:AF:E8:36:75  -66    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# *000004910 EB:C8:AF:E8:36:75  -65    02-01-06-02-0A-04-09-09-41-75-74-6F-53-74-6F-70
# ****CONNECT********
*
# Connected----
# Discovering MEWO Service...Found.
# Discovering MEWO Characteristic COMMAND...Found.
# Discovering MEWO Characteristic POSITION...Found.
# Ready to Receive Position Notificatinons

@ 870

@ 870
-*/
