# Overview

The project is the main demo application for the Silverado project.

The following things are demonstrated in this application:
- Build an application with multiple pages and navigating between them.
- Integrating widgets into pages.
- Change widget states & colors.
- Using contextual navigation buttons.

# Project Structure
- `src/main/java/`:
  - `com.microej.sbd`: starting point of the Application, containing the basic UI functionalities.
  - `com.microej.sbd.framework`: package containing classes for the button / command event framework and the desktop handling. 
  - `com.microej.sbd.pages`: package containing all the pages of the application.
  - `com.microej.sbd.resources`: package defining classes with constants pointing to the images / font resources. This package is used by all the classes that need to use resources.
  - `com.microej.sbd.widgets`: package containing all the widgets used by the application pages.
- `src/main/resources/`: all the resources used by the application (fonts, images...).

# Usage

## Switching between Landscape & Portrait mode

See the `README.rst` in the root folder of the Platform to understand how to build the right Platform for each orientation.

> **Note:**
> The change for the BSP has to be done whenever building the application for the specified orientation.
> It is not enough to have the change during building of the platform.

To start in Landscape mode, make sure you have already built the required platform and then start the required run configuration inside the `launchers` folder of this project.

Use `Main - Landscape (EMB).launch` for building on your board and `Main - Landscape (SIM).launch` for starting the Simulator.

To start in Portrait mode, make sure you have already built the required platform.
Open the project `com.microej.silverado.portrait` and start the required run configuration inside the `launchers` folder of that project.

Use `Main - Portrait (EMB).launch` for building on your board and `Main - Portrait (SIM).launch` for starting the Simulator.

# Requirements
This example has been tested on:  
MicroEJ SDK 5.4

With a platform that contains:
  -  EDC-1.3
  -  BON-1.4
  -  MICROUI-3.0
  -  DRAWING-1.0

# Dependencies

_All dependencies are retrieved transitively by MicroEJ Module Manager_.

# Source

N/A.

# Restrictions

None.

---
_Copyright 2021 MicroEJ Corp. All rights reserved._  
_This Software has been designed or modified by MicroEJ Corp._  
_MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software._  
