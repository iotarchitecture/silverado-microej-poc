/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.framework;

import com.microej.sbd.pages.CommonPage;

import ej.microui.event.Event;
import ej.microui.event.EventGenerator;
import ej.microui.event.generator.Command;
import ej.mwt.Desktop;
import ej.mwt.Widget;
import ej.mwt.stylesheet.CachedStylesheet;
import ej.mwt.stylesheet.Stylesheet;
import ej.mwt.stylesheet.cascading.CascadingStylesheet;

/**
 * Desktop implementation for the SBD Silverado project.
 */
public class SbdDesktop extends Desktop {

	/**
	 * Sets the stylesheet instance as a {@link CachedStylesheet}.
	 *
	 * @param stylesheet
	 *            the stylesheet instance.
	 */
	@Override
	public void setStylesheet(Stylesheet stylesheet) {
		super.setStylesheet(new CachedStylesheet(stylesheet));
	}

	/**
	 * Sets the stylesheet instance as a {@link CachedStylesheet}.
	 *
	 * @param stylesheet
	 *            the stylesheet instance.
	 *
	 * @deprecated Not for use. Use setStylesheet with a {@link CascadingStylesheet} instead.
	 */
	@Deprecated
	public void setStylesheet(CachedStylesheet stylesheet) {
		throw new IllegalArgumentException("Use setStylesheet with a CascadingStylesheet."); //$NON-NLS-1$
	}

	@Override
	public boolean handleEvent(int event) {
		if (Event.getType(event) == Command.EVENT_TYPE) {
			Widget widget = getWidget();
			if (widget instanceof CommonPage) {
				return ((CommonPage) widget).handleCommands(Event.getData(event));
			}
			return false;
		}
		return super.handleEvent(event);
	}

	/**
	 * Sends a command through the event handler.
	 *
	 * @param command
	 *            the command to send as int.
	 *
	 * @see Command
	 * @see ButtonCommands
	 */
	public static void sendCommand(int command) {
		Command commands = EventGenerator.get(Command.class, 0);
		if (commands != null) {
			commands.send(command);
		}
	}
}
