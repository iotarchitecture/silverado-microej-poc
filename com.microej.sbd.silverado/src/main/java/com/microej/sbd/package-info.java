/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
/**
 * Contains the main classes for UI startup.
 */
@ej.annotation.NonNullByDefault
package com.microej.sbd;