/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import com.microej.sbd.resources.Images;

import ej.annotation.Nullable;
import ej.microui.display.GraphicsContext;
import ej.microui.display.Painter;
import ej.microui.display.ResourceImage;
import ej.mwt.Widget;
import ej.mwt.style.Style;
import ej.mwt.util.Size;

/**
 * Draws a horizontal dotted line with arrow heads on left and right.
 */
public class HorizontalArrow extends Widget {

	/** Extra style id for line thickness. */
	public static final int STYLE_THICKNESS = 0;
	/** Extra style id for dot length. */
	public static final int STYLE_DOT_LENGTH = 1;
	/** Extra style id for dot thickness. */
	public static final int STYLE_DOT_SPACING = 2;

	private static final int DEFAULT_THICKNESS = 4;
	private static final int DEFAULT_DOT_LENGTH = 16;
	private static final int DEFAULT_DOT_SPACING = 10;

	private @Nullable ResourceImage leftArrowImage;
	private @Nullable ResourceImage rightArrowImage;

	@Override
	protected void onAttached() {
		super.onAttached();

		this.leftArrowImage = ResourceImage.loadImage(Images.LEFT_ARROW);
		this.rightArrowImage = ResourceImage.loadImage(Images.RIGHT_ARROW);
	}

	@Override
	protected void onDetached() {
		super.onDetached();

		if (this.leftArrowImage != null) {
			this.leftArrowImage.close();
		}
		if (this.rightArrowImage != null) {
			this.rightArrowImage.close();
		}
	}

	@Override
	protected void computeContentOptimalSize(Size size) {
		int lineThickness = getStyle().getExtraInt(STYLE_THICKNESS, DEFAULT_THICKNESS);

		final ResourceImage leftArrowImg = this.leftArrowImage;
		final ResourceImage rigthArrowImg = this.rightArrowImage;
		if (leftArrowImg != null && rigthArrowImg != null) {
			int width = leftArrowImg.getWidth() + lineThickness + rigthArrowImg.getWidth();
			int height = Math.max(leftArrowImg.getHeight(), rigthArrowImg.getHeight());
			size.setSize(width, height);
		}
	}

	@Override
	protected void renderContent(GraphicsContext g, int contentWidth, int contentHeight) {
		Style style = getStyle();
		int lineThickness = style.getExtraInt(STYLE_THICKNESS, DEFAULT_THICKNESS);
		int dotLength = style.getExtraInt(STYLE_DOT_LENGTH, DEFAULT_DOT_LENGTH);
		int dotSpacing = style.getExtraInt(STYLE_DOT_SPACING, DEFAULT_DOT_SPACING);
		int dotPeriod = dotLength + dotSpacing;

		final ResourceImage leftArrowImg = this.leftArrowImage;
		final ResourceImage rigthArrowImg = this.rightArrowImage;
		if (leftArrowImg != null && rigthArrowImg != null) {
			int leftWidth = leftArrowImg.getWidth();
			int leftHeight = leftArrowImg.getHeight();
			int rightWidth = rigthArrowImg.getWidth();
			int rightHeight = rigthArrowImg.getHeight();

			g.setColor(style.getColor());
			Painter.drawImage(g, leftArrowImg, 0, contentHeight / 2 - leftHeight / 2);
			Painter.drawImage(g, rigthArrowImg, contentWidth - rightWidth, contentHeight / 2 - rightHeight / 2);

			int lineLength = contentWidth - (leftWidth + rightWidth);
			int missed = (lineLength - dotLength + dotPeriod - 1) / dotPeriod * dotPeriod + dotLength - lineLength;
			g.intersectClip(leftWidth, 0, lineLength, contentHeight);

			int dotOffset = -missed / 2;
			while (dotOffset < lineLength) {
				Painter.fillRectangle(g, leftWidth + dotOffset, contentHeight / 2 - lineThickness / 2, dotLength,
						lineThickness);
				dotOffset += dotPeriod;
			}
		}
	}
}
