/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import com.microej.sbd.resources.Images;

import ej.microui.display.GraphicsContext;
import ej.mwt.style.Style;
import ej.widget.basic.ImageWidget;
import ej.widget.util.render.StringPainter;

/**
 * Type widget for the different cut angles.
 */
public class TypeWidget extends ImageWidget {

	/** Cut angle index for type A. */
	public static final int TYPE_A = 0;
	/** Cut angle index for type B. */
	public static final int TYPE_B = 1;
	/** Cut angle index for type C. */
	public static final int TYPE_C = 2;
	/** Number of different cut angle types. */
	public static final int NUM_TYPES = 3;

	private int type;

	/**
	 * Creates the TypeWidget.
	 *
	 * @param type
	 *            the type to display.
	 */
	public TypeWidget(int type) {
		super(getImagePath(type));
		this.type = type;
	}

	/**
	 * Gets the current type.
	 *
	 * @return index of the current type.
	 */
	public int getType() {
		return this.type;
	}

	/**
	 * Sets the type displayed.
	 *
	 * @param type
	 *            the index of type to set.
	 */
	public void setType(int type) {
		this.type = type;
		setImagePath(getImagePath(type));
	}

	@Override
	protected void renderContent(GraphicsContext g, int contentWidth, int contentHeight) {
		super.renderContent(g, contentWidth, contentHeight);

		Style style = getStyle();
		g.setColor(style.getColor());
		g.removeBackgroundColor();
		String text = getText(this.type);
		StringPainter.drawStringInArea(g, text, style.getFont(), 0, 0, contentWidth, contentHeight,
				style.getHorizontalAlignment(), style.getVerticalAlignment());
	}

	private static String getImagePath(int type) {
		switch (type) {
		case TYPE_A:
			return Images.TYPE_A;
		case TYPE_B:
			return Images.TYPE_B;
		case TYPE_C:
			return Images.TYPE_C;
		default:
			throw new IllegalArgumentException();
		}
	}

	private static String getText(int type) {
		switch (type) {
		case TYPE_A:
			return "A"; //$NON-NLS-1$
		case TYPE_B:
			return "B"; //$NON-NLS-1$
		case TYPE_C:
			return "C"; //$NON-NLS-1$
		default:
			throw new IllegalArgumentException();
		}
	}
}
