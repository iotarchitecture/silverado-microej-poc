/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

/**
 * LengthInputField that is fixed to a maximum number of digits.
 */
public class LengthInputField extends InputField {

	private static final String UNIT_IN = " in"; //$NON-NLS-1$
	private static final int TEN_MULTIPLIER = 10;

	private final int maxDigits;
	private final int defaultValue;

	private int value;

	/**
	 * Creates the LengthInput Field.
	 *
	 * @param maxDigits
	 *            the maximum number of digits to input.
	 * @param defaultValue
	 *            the default value to display.
	 */
	public LengthInputField(int maxDigits, int defaultValue) {
		super(computePattern(maxDigits));
		this.maxDigits = maxDigits;
		this.defaultValue = defaultValue;
		this.value = defaultValue;
		updateText();
	}

	/**
	 * Gets the current value.
	 *
	 * @return the value.
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * Sets the value to display.
	 *
	 * @param value
	 *            the value to set.
	 */
	public void setValue(int value) {
		this.value = value;
		updateText();
	}

	private void updateText() {
		setText(this.value + UNIT_IN);
	}

	@Override
	protected void addDigit(int digit) {
		int numDigits = Integer.toString(this.value).length();
		if (numDigits + 1 <= this.maxDigits) {
			this.value = this.value * TEN_MULTIPLIER + digit;
			updateText();
			requestRender();
		}
	}

	@Override
	protected void removeDigit() {
		this.value /= TEN_MULTIPLIER;
		updateText();
		requestRender();
	}

	@Override
	protected void clear() {
		this.value = 0;
		updateText();
		requestRender();
	}

	@Override
	protected void reset() {
		this.value = this.defaultValue;
		updateText();
		requestRender();
	}

	private static String computePattern(int maxDigits) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < maxDigits; i++) {
			stringBuilder.append('0');
		}
		stringBuilder.append(UNIT_IN);
		return stringBuilder.toString();
	}
}
