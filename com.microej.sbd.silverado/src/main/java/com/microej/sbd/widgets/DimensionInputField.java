/*
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import com.microej.sbd.framework.ButtonCommands;

/**
 * DimensionInputField with common length units.
 */
public class DimensionInputField extends InputField {

	/** Index for unit FT. */
	public static final int UNIT_INDEX_FT = 0;
	/** Index for unit IN. */
	public static final int UNIT_INDEX_IN = 1;
	/** Index for unit CM. */
	public static final int UNIT_INDEX_CM = 2;
	/** Index for unit MM. */
	public static final int UNIT_INDEX_MM = 3;

	private static final String[] UNITS = { "ft", "in", "cm", "mm" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	private static final int TEN_MULTIPLIER = 10;
	private static final int QUARTER_MOD = 4;

	private final int maxIntDigits;
	private final int defaultIntValue;
	private final int defaultDecValue;
	private final int defaultUnitIndex;

	private int intValue;
	private int decValue;
	private int unitIndex;

	/**
	 * Creates DimensionInputField.
	 *
	 * @param maxIntDigits
	 *            the maximum number of digits for the input.
	 * @param defaultIntValue
	 *            the default value to display.
	 * @param defaultDecValue
	 *            the default decimal value to display.
	 * @param defaultUnitIndex
	 *            the default unit index to use.
	 */
	public DimensionInputField(int maxIntDigits, int defaultIntValue, int defaultDecValue, int defaultUnitIndex) {
		super(computePattern(maxIntDigits));
		this.maxIntDigits = maxIntDigits;
		this.defaultIntValue = defaultIntValue;
		this.defaultDecValue = defaultDecValue;
		this.defaultUnitIndex = defaultUnitIndex;
		this.intValue = defaultIntValue;
		this.decValue = defaultDecValue;
		this.unitIndex = defaultUnitIndex;
		updateText();
	}

	private void updateText() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(this.intValue).append(' ');
		if (this.decValue != 0) {
			stringBuilder.append(this.decValue).append("/4 "); //$NON-NLS-1$
		}
		stringBuilder.append(UNITS[this.unitIndex]);
		setText(stringBuilder.toString());
	}

	@Override
	protected void addDigit(int digit) {
		int numDigits = Integer.toString(this.intValue).length();
		if (numDigits + 1 <= this.maxIntDigits) {
			this.intValue = this.intValue * TEN_MULTIPLIER + digit;
			updateText();
			requestRender();
		}
	}

	@Override
	protected void removeDigit() {
		this.intValue /= TEN_MULTIPLIER;
		updateText();
		requestRender();
	}

	private void addEquation() {
		this.decValue = (this.decValue + 1) % QUARTER_MOD;
		updateText();
		requestRender();
	}

	@Override
	protected void clear() {
		this.intValue = 0;
		this.decValue = 0;
		updateText();
		requestRender();
	}

	@Override
	protected void reset() {
		this.intValue = this.defaultIntValue;
		this.decValue = this.defaultDecValue;
		this.unitIndex = this.defaultUnitIndex;
		updateText();
		requestRender();
	}

	private void switchUnit() {
		this.unitIndex = (this.unitIndex + 1) % UNITS.length;
		updateText();
		requestRender();
	}

	/**
	 * Handle commands of button presses.
	 *
	 * @param command
	 *            the command to handle.
	 *
	 * @return <code>true</code> if the widget has consumed the event, <code>false</code> otherwise.
	 */
	@Override
	public boolean handleButtonPress(int command) {
		if (command == ButtonCommands.CONTEXT_ZERO) {
			switchUnit();
			return true;
		} else if (command == ButtonCommands.CONTEXT_TWO) {
			addEquation();
			return true;
		}

		return super.handleButtonPress(command);
	}

	private static String computePattern(int maxIntDigits) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < maxIntDigits; i++) {
			stringBuilder.append('0');
		}
		stringBuilder.append(" 0/0 mm"); //$NON-NLS-1$
		return stringBuilder.toString();
	}
}
