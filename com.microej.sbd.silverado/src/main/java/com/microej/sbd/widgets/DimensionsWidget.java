/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import com.microej.sbd.resources.ClassSelectors;

import ej.mwt.Widget;
import ej.widget.container.LayoutOrientation;
import ej.widget.container.List;

/** Widget to display the dimensions and to edit them. */
public class DimensionsWidget extends List {

	/** The X dimension index. */
	public static final int DIMENSION_X = 0;
	/** The Y dimension index. */
	public static final int DIMENSION_Y = 1;
	/** The number of dimensions. */
	public static final int NUM_DIMENSIONS = 2;

	private final TypeWidget typeWidget;
	private final PatternLabel xDimensionLabel;
	private final PatternLabel yDimensionLabel;
	private final HorizontalArrow xDimensionArrow;
	private final VerticalArrow yDimensionArrow;

	private int selectedDimension;
	private int xDimensionValue;
	private int yDimensionValue;

	/**
	 * Creates the DimensionsWidget.
	 *
	 * @param type
	 *            the {@link TypeWidget} angular cut type to display.
	 * @param selectedDimension
	 *            the dimension that is currently selected. Either {@link #DIMENSION_X} or {@link #DIMENSION_Y}.
	 * @param xDimensionValue
	 *            the current value set for the X dimension.
	 * @param yDimensionValue
	 *            the current value set for the Y dimension.
	 * @param maxDigits
	 *            the maximum number of digits to use in the {@link PatternLabel}.
	 */
	public DimensionsWidget(int type, int selectedDimension, int xDimensionValue, int yDimensionValue, int maxDigits) {
		super(LayoutOrientation.HORIZONTAL);

		this.selectedDimension = selectedDimension;
		this.xDimensionValue = xDimensionValue;
		this.yDimensionValue = yDimensionValue;

		this.typeWidget = new TypeWidget(type);

		String pattern = computePattern(maxDigits);

		this.xDimensionLabel = new PatternLabel(pattern, Integer.toString(xDimensionValue) + '"');
		this.xDimensionLabel.addClassSelector(ClassSelectors.DIMENSION);
		this.xDimensionLabel.addClassSelector(ClassSelectors.X_DIMENSION_LABEL);

		this.yDimensionLabel = new PatternLabel(pattern, Integer.toString(yDimensionValue) + '"');
		this.yDimensionLabel.addClassSelector(ClassSelectors.DIMENSION);
		this.yDimensionLabel.addClassSelector(ClassSelectors.Y_DIMENSION_LABEL);

		this.xDimensionArrow = new HorizontalArrow();
		this.xDimensionArrow.addClassSelector(ClassSelectors.DIMENSION);

		this.yDimensionArrow = new VerticalArrow();
		this.yDimensionArrow.addClassSelector(ClassSelectors.DIMENSION);

		List xDimensionList = new List(LayoutOrientation.VERTICAL);
		xDimensionList.addClassSelector(ClassSelectors.X_DIMENSION_LIST);
		if (type == TypeWidget.TYPE_A) {
			xDimensionList.addClassSelector(ClassSelectors.X_DIMENSION_LIST_A);
		} else if (type == TypeWidget.TYPE_B) {
			xDimensionList.addClassSelector(ClassSelectors.X_DIMENSION_LIST_B);
		} else if (type == TypeWidget.TYPE_C) {
			xDimensionList.addClassSelector(ClassSelectors.X_DIMENSION_LIST_C);
		}
		xDimensionList.addChild(this.xDimensionArrow);
		xDimensionList.addChild(this.xDimensionLabel);

		List verticalList = new List(LayoutOrientation.VERTICAL);
		verticalList.addChild(this.typeWidget);
		verticalList.addChild(xDimensionList);

		List yDimensionList = new List(LayoutOrientation.HORIZONTAL);
		yDimensionList.addClassSelector(ClassSelectors.Y_DIMENSION_LIST);
		yDimensionList.addChild(this.yDimensionLabel);
		yDimensionList.addChild(this.yDimensionArrow);

		super.addChild(yDimensionList);
		super.addChild(verticalList);

		setDimensionStyle(selectedDimension, true);
	}

	/**
	 * Gets the currently selected dimension.
	 *
	 * @return the selected dimension. Either {@link #DIMENSION_X} or {@link #DIMENSION_Y}.
	 */
	public int getSelectedDimension() {
		return this.selectedDimension;
	}

	/**
	 * Gets the value for the given dimension.
	 *
	 * @param dimension
	 *            the dimension for which to get the value. Either {@link #DIMENSION_X} or {@link #DIMENSION_Y}.
	 * @return the value.
	 */
	public int getDimensionValue(int dimension) {
		switch (dimension) {
		case DIMENSION_X:
			return this.xDimensionValue;
		case DIMENSION_Y:
			return this.yDimensionValue;
		default:
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Sets the currently selected dimension.
	 *
	 * @param selectedDimension
	 *            the dimension to select. Either {@link #DIMENSION_X} or {@link #DIMENSION_Y}.
	 */
	public void setSelectedDimension(int selectedDimension) {
		int oldSelectedDimension = this.selectedDimension;
		this.selectedDimension = selectedDimension;

		setDimensionStyle(oldSelectedDimension, false);
		setDimensionStyle(selectedDimension, true);
	}

	/**
	 * Sets the value for the given dimension.
	 *
	 * @param dimension
	 *            the dimension for which to set the value. Either {@link #DIMENSION_X} or {@link #DIMENSION_Y}.
	 * @param value
	 *            the value to set.
	 */
	public void setDimensionValue(int dimension, int value) {
		switch (dimension) {
		case DIMENSION_X:
			this.xDimensionValue = value;
			this.xDimensionLabel.setText(Integer.toString(value) + '"');
			break;
		case DIMENSION_Y:
			this.yDimensionValue = value;
			this.yDimensionLabel.setText(Integer.toString(value) + '"');
			break;
		default:
			throw new IllegalArgumentException();
		}
	}

	private void setDimensionStyle(int dimension, boolean selected) {
		switch (dimension) {
		case DIMENSION_X:
			setWidgetStyle(this.xDimensionLabel, selected);
			setWidgetStyle(this.xDimensionArrow, selected);
			break;
		case DIMENSION_Y:
			setWidgetStyle(this.yDimensionLabel, selected);
			setWidgetStyle(this.yDimensionArrow, selected);
			break;
		default:
			// do nothing
		}
	}

	private void setWidgetStyle(Widget widget, boolean selected) {
		if (selected) {
			if (!widget.hasClassSelector(ClassSelectors.SELECTED_DIMENSION)) {
				widget.addClassSelector(ClassSelectors.SELECTED_DIMENSION);
			}
		} else {
			widget.removeClassSelector(ClassSelectors.SELECTED_DIMENSION);
		}
		widget.updateStyle();
	}

	private static String computePattern(int maxDigits) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < maxDigits; i++) {
			stringBuilder.append('0');
		}
		stringBuilder.append('"');
		return stringBuilder.toString();
	}
}
