/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import com.microej.sbd.framework.ButtonCommands;

import ej.microui.display.Font;
import ej.microui.display.GraphicsContext;
import ej.microui.display.Painter;
import ej.mwt.Widget;
import ej.mwt.style.Style;
import ej.mwt.util.Alignment;
import ej.mwt.util.Size;
import ej.widget.util.render.StringPainter;

/**
 * InputField that is restricted to a pattern size and is underlined.
 */
public abstract class InputField extends Widget {

	/** Extra style id for line thickness. */
	public static final int STYLE_THICKNESS = 0;

	private static final int DEFAULT_LINE_THICKNESS = 2;

	private final String pattern;

	private String text;

	/**
	 * Creates the InputField.
	 *
	 * @param pattern
	 *            the pattern to size the field.
	 */
	public InputField(String pattern) {
		this.pattern = pattern;
		this.text = ""; //$NON-NLS-1$
	}

	/**
	 * Gets the displayed text.
	 *
	 * @return the text.
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * Sets the text to display.
	 *
	 * @param text
	 *            the text to display.
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Handle commands of button presses.
	 *
	 * @param command
	 *            the command to handle.
	 *
	 * @return <code>true</code> if the widget has consumed the event, <code>false</code> otherwise.
	 */
	public boolean handleButtonPress(int command) {
		if (command >= ButtonCommands.DIGIT_ZERO && command <= ButtonCommands.DIGIT_NINE) {
			int digit = ButtonCommands.getDigitFromCommand(command);
			addDigit(digit);
			return true;
		} else if (command == ButtonCommands.CLEAR) {
			clear();
			return true;
		} else if (command == ButtonCommands.FIX) {
			removeDigit();
			return true;
		} else if (command == ButtonCommands.RESET) {
			reset();
			return true;
		}

		return false;
	}

	/**
	 * Adds a digit to the input field.
	 *
	 * @param digit
	 *            the digit to add.
	 */
	protected abstract void addDigit(int digit);

	/**
	 * Clears the input field.
	 */
	protected abstract void clear();

	/**
	 * Removes a digit from the input field.
	 */
	protected abstract void removeDigit();

	/**
	 * Resets the input field to default.
	 */
	protected abstract void reset();

	@Override
	protected void computeContentOptimalSize(Size size) {
		Style style = getStyle();
		int lineThickness = style.getExtraInt(STYLE_THICKNESS, DEFAULT_LINE_THICKNESS);
		Font font = style.getFont();
		int width = font.stringWidth(this.pattern);
		int height = font.getHeight() + lineThickness;
		size.setSize(width, height);
	}

	@Override
	protected void renderContent(GraphicsContext g, int contentWidth, int contentHeight) {
		Style style = getStyle();
		int lineThickness = style.getExtraInt(STYLE_THICKNESS, DEFAULT_LINE_THICKNESS);
		Font font = style.getFont();
		int fontHeight = font.getHeight();

		g.setColor(style.getColor());
		StringPainter.drawStringInArea(g, this.text, font, 0, 0, contentWidth, contentHeight,
				style.getHorizontalAlignment(), style.getVerticalAlignment());

		int lineY = Alignment.computeTopY(fontHeight, fontHeight, contentHeight, style.getVerticalAlignment());
		Painter.fillRectangle(g, 0, lineY, contentWidth, lineThickness);
	}
}
