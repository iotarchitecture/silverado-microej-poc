/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import com.microej.sbd.resources.Images;

import ej.annotation.Nullable;
import ej.microui.display.GraphicsContext;
import ej.microui.display.Painter;
import ej.microui.display.ResourceImage;
import ej.mwt.Widget;
import ej.mwt.style.Style;
import ej.mwt.util.Size;

/**
 * Draws a vertical dotted line with arrow heads on top and bottom.
 */
public class VerticalArrow extends Widget {

	/** Extra style id for line thickness. */
	public static final int STYLE_THICKNESS = 0;
	/** Extra style id for dot length. */
	public static final int STYLE_DOT_LENGTH = 1;
	/** Extra style id for dot thickness. */
	public static final int STYLE_DOT_SPACING = 2;

	private static final int DEFAULT_THICKNESS = 4;
	private static final int DEFAULT_DOT_LENGTH = 9;
	private static final int DEFAULT_DOT_SPACING = 6;

	private @Nullable ResourceImage upArrowImage;
	private @Nullable ResourceImage downArrowImage;

	@Override
	protected void onAttached() {
		super.onAttached();

		this.upArrowImage = ResourceImage.loadImage(Images.UP_ARROW);
		this.downArrowImage = ResourceImage.loadImage(Images.DOWN_ARROW);
	}

	@Override
	protected void onDetached() {
		super.onDetached();

		if (this.upArrowImage != null) {
			this.upArrowImage.close();
		}
		if (this.downArrowImage != null) {
			this.downArrowImage.close();
		}
	}

	@Override
	protected void computeContentOptimalSize(Size size) {
		int lineThickness = getStyle().getExtraInt(STYLE_THICKNESS, DEFAULT_THICKNESS);

		final ResourceImage upArrowImg = this.upArrowImage;
		final ResourceImage downArrowImg = this.downArrowImage;
		if (upArrowImg != null && downArrowImg != null) {
			int width = Math.max(upArrowImg.getWidth(), downArrowImg.getWidth());
			int height = upArrowImg.getHeight() + lineThickness + downArrowImg.getHeight();
			size.setSize(width, height);
		}
	}

	@Override
	protected void renderContent(GraphicsContext g, int contentWidth, int contentHeight) {
		Style style = getStyle();
		int lineThickness = style.getExtraInt(STYLE_THICKNESS, DEFAULT_THICKNESS);
		int dotLength = style.getExtraInt(STYLE_DOT_LENGTH, DEFAULT_DOT_LENGTH);
		int dotSpacing = style.getExtraInt(STYLE_DOT_SPACING, DEFAULT_DOT_SPACING);
		int dotPeriod = dotLength + dotSpacing;

		final ResourceImage upArrowImg = this.upArrowImage;
		final ResourceImage downArrowImg = this.downArrowImage;
		if (upArrowImg != null && downArrowImg != null) {
			int upWidth = upArrowImg.getWidth();
			int upHeight = upArrowImg.getHeight();
			int downWidth = downArrowImg.getWidth();
			int downHeight = downArrowImg.getHeight();

			g.setColor(style.getColor());
			Painter.drawImage(g, upArrowImg, contentWidth / 2 - upWidth / 2, 0);
			Painter.drawImage(g, downArrowImg, contentWidth / 2 - downWidth / 2, contentHeight - downHeight);

			int lineLength = contentHeight - (upHeight + downHeight);
			int missed = (lineLength - dotLength + dotPeriod - 1) / dotPeriod * dotPeriod + dotLength - lineLength;
			g.intersectClip(0, upHeight, contentWidth, lineLength);

			int dotOffset = -missed / 2;
			while (dotOffset < lineLength) {
				Painter.fillRectangle(g, contentWidth / 2 - lineThickness / 2, upHeight + dotOffset, lineThickness,
						dotLength);
				dotOffset += dotPeriod;
			}
		}
	}
}
