/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import com.microej.sbd.framework.ButtonCommands;

import ej.widget.container.Grid;
import ej.widget.container.LayoutOrientation;

/**
 * NavigationFooter that is displayed at the bottom of the page to describe the context buttons actions.
 */
public class NavigationFooter extends Grid {

	private static final int NAV_INDEX_CONTEXT_ZERO = 0;
	private static final int NAV_INDEX_CONTEXT_ONE = 1;
	private static final int NAV_INDEX_CONTEXT_TWO = 2;
	private static final int NAV_INDEX_CONTEXT_THREE = 3;
	private static final int NUM_INDICATORS = 4;

	private final NavigationInfo[] infoWidgets;

	/**
	 * Creates the NavigationFooter.
	 */
	public NavigationFooter() {
		super(LayoutOrientation.HORIZONTAL, NUM_INDICATORS);

		this.infoWidgets = new NavigationInfo[NUM_INDICATORS];
		for (int i = 0; i < NUM_INDICATORS; i++) {
			NavigationInfo infoWidget = new NavigationInfo(ButtonCommands.CONTEXT_ZERO + i);
			this.infoWidgets[i] = infoWidget;
			super.addChild(infoWidget);
		}
	}

	/**
	 * Sets info to display for all of the context buttons.
	 *
	 * @param textLeft
	 *            the text to set for the left most context button.
	 * @param textCenterLeft
	 *            the text to set for the center left context button.
	 * @param textCenterRight
	 *            the text to set for the center right context button.
	 * @param textRight
	 *            the text to set for the right most context button.
	 */
	public void setInfo(String textLeft, String textCenterLeft, String textCenterRight, String textRight) {
		this.infoWidgets[NAV_INDEX_CONTEXT_ZERO].setText(textLeft);
		this.infoWidgets[NAV_INDEX_CONTEXT_ONE].setText(textCenterLeft);
		this.infoWidgets[NAV_INDEX_CONTEXT_TWO].setText(textCenterRight);
		this.infoWidgets[NAV_INDEX_CONTEXT_THREE].setText(textRight);
	}

	/**
	 * Sets the info to display for the left most context button.
	 *
	 * @param text
	 *            the text to set.
	 */
	public void setInfoLeft(String text) {
		this.infoWidgets[NAV_INDEX_CONTEXT_ZERO].setText(text);
	}

	/**
	 * Sets the info to display for the center left context button.
	 *
	 * @param text
	 *            the text to set.
	 */
	public void setInfoCenterLeft(String text) {
		this.infoWidgets[NAV_INDEX_CONTEXT_ONE].setText(text);
	}

	/**
	 * Sets the info to display for the center right context button.
	 *
	 * @param text
	 *            the text to set.
	 */
	public void setInfoCenterRight(String text) {
		this.infoWidgets[NAV_INDEX_CONTEXT_TWO].setText(text);
	}

	/**
	 * Sets the info to display for the right most context button.
	 *
	 * @param text
	 *            the text to set.
	 */
	public void setInfoRight(String text) {
		this.infoWidgets[NAV_INDEX_CONTEXT_THREE].setText(text);
	}

}
