/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import com.microej.sbd.resources.ClassSelectors;
import com.microej.sbd.resources.Images;

import ej.widget.basic.ImageWidget;
import ej.widget.container.LayoutOrientation;
import ej.widget.container.SimpleDock;

/**
 * The status header displayed on the top of the Pages.
 */
public class StatusHeader extends SimpleDock {

	/**
	 * Creates the StatusHeader.
	 */
	public StatusHeader() {
		super(LayoutOrientation.HORIZONTAL);

		ImageWidget bluetoothImageWidget = new ImageWidget(Images.BLUETOOTH);
		bluetoothImageWidget.addClassSelector(ClassSelectors.BLUETOOTH_IMAGE_WIDGET);
		super.setFirstChild(bluetoothImageWidget);

		super.setCenterChild(new ImageWidget(Images.LOGO_SMALL));

		ImageWidget batteryImageWidget = new ImageWidget(Images.BATTERY);
		batteryImageWidget.addClassSelector(ClassSelectors.BATTERY_IMAGE_WIDGET);
		super.setLastChild(batteryImageWidget);
	}
}
