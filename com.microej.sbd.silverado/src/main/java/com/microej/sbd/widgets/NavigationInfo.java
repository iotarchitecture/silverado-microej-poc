/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import com.microej.sbd.framework.SbdDesktop;

import ej.microui.display.GraphicsContext;
import ej.microui.event.Event;
import ej.microui.event.generator.Buttons;
import ej.microui.event.generator.Pointer;
import ej.mwt.Widget;
import ej.widget.basic.Label;
import ej.widget.container.LayoutOrientation;
import ej.widget.container.SimpleDock;

/**
 * NavigationInfo widget to display the current function of the context buttons.
 */
public class NavigationInfo extends SimpleDock {

	private final int command;
	private final Label label;
	private final NavigationIndicator indicator;

	/**
	 * Creates the NavigationInfo widget.
	 *
	 * @param command
	 *            command to send when clicked.
	 */
	public NavigationInfo(int command) {
		super(LayoutOrientation.VERTICAL);
		super.setEnabled(true);

		this.command = command;
		this.label = new Label();
		this.indicator = new NavigationIndicator();

		super.setFirstChild(this.label);
		super.setCenterChild(this.indicator);
	}

	@Override
	public boolean handleEvent(int event) {
		if (Event.getType(event) == Pointer.EVENT_TYPE && Buttons.isReleased(event)) {
			SbdDesktop.sendCommand(this.command);
		}

		return super.handleEvent(event);
	}

	@Override
	protected void setShownChildren() {
		setShownChild(this.label);

		if (!this.label.getText().isEmpty()) {
			setShownChild(this.indicator);
		}
	}

	@Override
	protected void renderChild(Widget child, GraphicsContext g) {
		if (child.isShown()) {
			super.renderChild(child, g);
		}
	}

	/**
	 * Sets the text of the widget.
	 *
	 * @param text
	 *            the text to display.
	 */
	public void setText(String text) {
		this.label.setText(text);
	}
}
