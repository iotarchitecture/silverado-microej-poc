/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import ej.microui.display.Font;
import ej.mwt.util.Size;
import ej.widget.basic.Label;
import ej.widget.util.render.StringPainter;

/**
 * Label that adjusts size for a given pattern.
 */
public class PatternLabel extends Label {

	private final String pattern;

	/**
	 * Creates a PatternLabel.
	 *
	 * @param pattern
	 *            the pattern to use.
	 */
	public PatternLabel(String pattern) {
		this(pattern, ""); //$NON-NLS-1$
	}

	/**
	 * Creates a PatternLabel.
	 *
	 * @param pattern
	 *            the pattern to use.
	 * @param text
	 *            the text to display.
	 */
	public PatternLabel(String pattern, String text) {
		super(text);
		this.pattern = pattern;
	}

	@Override
	protected void computeContentOptimalSize(Size size) {
		Font font = getStyle().getFont();
		StringPainter.computeOptimalSize(this.pattern, font, size);
	}
}
