/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.widgets;

import ej.microui.display.GraphicsContext;
import ej.microui.display.Painter;
import ej.mwt.Widget;
import ej.mwt.style.Style;
import ej.mwt.util.Size;

/**
 * NavigationIndicator widget that displays a T-shape for the context buttons navigation.
 */
public class NavigationIndicator extends Widget {

	/** Extra style id for indicator thickness. */
	public static final int STYLE_THICKNESS = 0;

	private static final int DEFAULT_THICKNESS = 4;

	private static final int MIN_HORIZONTAL_THICKNESS_MULTIPLIER = 3;
	private static final int MIN_VERTICAL_THICKNESS_MULTIPLIER = 3;

	@Override
	protected void computeContentOptimalSize(Size size) {
		int lineThickness = getStyle().getExtraInt(STYLE_THICKNESS, DEFAULT_THICKNESS);
		size.setSize(MIN_HORIZONTAL_THICKNESS_MULTIPLIER * lineThickness,
				MIN_VERTICAL_THICKNESS_MULTIPLIER * lineThickness); // recognizable T shape
	}

	@Override
	protected void renderContent(GraphicsContext g, int contentWidth, int contentHeight) {
		Style style = getStyle();
		int lineThickness = style.getExtraInt(STYLE_THICKNESS, DEFAULT_THICKNESS);

		g.setColor(style.getColor());
		Painter.fillRectangle(g, 0, 0, contentWidth, lineThickness);
		Painter.fillRectangle(g, contentWidth / 2 - lineThickness / 2, lineThickness, lineThickness,
				contentHeight - lineThickness);
	}
}
