/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd;

import com.microej.sbd.framework.SbdDesktop;
import com.microej.sbd.framework.ScreenSize;
import com.microej.sbd.pages.CommonPage;
import com.microej.sbd.pages.WelcomePage;

import ej.annotation.Nullable;
import ej.microui.MicroUI;

/**
 * Main class.
 */
public class Main {
	private static @Nullable SbdDesktop desktop;

	/**
	 * Main method.
	 *
	 * @param args
	 *            unused.
	 */
	public static void main(String[] args) {
		MicroUI.start();

		desktop = new SbdDesktop();
		desktop.setStylesheet(StylesheetBuilder.build(ScreenSize.getWidth(), ScreenSize.getHeight()));
		assert (desktop != null);
		desktop.setWidget(new WelcomePage());
		assert (desktop != null);
		desktop.requestShow();
	}

	/**
	 * Displays the given {@link CommonPage}.
	 *
	 * @param page
	 *            the Page to show.
	 */
	public static void showPage(CommonPage page) {
		assert (desktop != null);
		desktop.setWidget(page);
		assert (desktop != null);
		desktop.requestLayOut();
	}
}
