/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd;

import com.microej.sbd.resources.ClassSelectors;
import com.microej.sbd.resources.Fonts;
import com.microej.sbd.widgets.DimensionsWidget;
import com.microej.sbd.widgets.HorizontalArrow;
import com.microej.sbd.widgets.InputField;
import com.microej.sbd.widgets.NavigationIndicator;
import com.microej.sbd.widgets.NavigationInfo;
import com.microej.sbd.widgets.StatusHeader;
import com.microej.sbd.widgets.TypeWidget;
import com.microej.sbd.widgets.VerticalArrow;

import ej.microui.display.Colors;
import ej.mwt.Widget;
import ej.mwt.style.EditableStyle;
import ej.mwt.style.background.RectangularBackground;
import ej.mwt.style.dimension.FixedDimension;
import ej.mwt.style.dimension.OptimalDimension;
import ej.mwt.style.outline.FlexibleOutline;
import ej.mwt.style.outline.UniformOutline;
import ej.mwt.stylesheet.cascading.CascadingStylesheet;
import ej.mwt.stylesheet.selector.ClassSelector;
import ej.mwt.stylesheet.selector.TypeSelector;
import ej.mwt.stylesheet.selector.combinator.AndCombinator;
import ej.mwt.util.Alignment;

/**
 * Loads all the style definitions into a {@link CascadingStylesheet}. This should usually be called only once at
 * start-up, unless the styles change dynamically.
 */
public class StylesheetBuilder {
	private static final int PRIMARY_COLOR = 0xF5BA10;
	private static final int SECONDARY_COLOR = 0xD3D3D3;
	private static final int BLUETOOTH_COLOR = 0x4071D1;

	private static final int HUNDRED_PERCENT = 100;
	private static final float ROUND = 0.5f;

	private static final float REFERENCE_WIDTH = 480;
	private static final float REFERENCE_HEIGHT = 272;

	private static final float SELECTED_SAW_LIST_MARGIN_HORIZONTAL = 30 / REFERENCE_WIDTH;
	private static final float STATUS_HEADER_PADDING = 5 / REFERENCE_WIDTH;
	private static final float NAVIGATION_INFO_PADDING_HORIZONTAL = 5 / REFERENCE_WIDTH;
	private static final float UNSELECTED_ARROW_WIDTH = 150 / REFERENCE_WIDTH;
	private static final float SELECTED_ARROW_WIDTH = 280 / REFERENCE_WIDTH;
	private static final float Y_DIMENSION_LIST_MARGIN_RIGHT = 8 / REFERENCE_WIDTH;
	private static final float Y_DIMENSION_LABEL_MARGIN_RIGHT = 5 / REFERENCE_WIDTH;
	private static final float X_DIMENSION_LIST_A_WIDTH = 244 / REFERENCE_WIDTH;
	private static final float X_DIMENSION_LIST_B_WIDTH = 188 / REFERENCE_WIDTH;
	private static final float X_DIMENSION_LIST_C_WIDTH = 244 / REFERENCE_WIDTH;
	private static final float HORIZONTAL_ARROW_DOT_LENGTH = 16 / REFERENCE_WIDTH;
	private static final float HORIZONTAL_ARROW_DOT_SPACING = 10 / REFERENCE_WIDTH;
	private static final float VERTICAL_ARROW_THICKNESS = 4 / REFERENCE_WIDTH;

	private static final float NAVIGATION_INDICATOR_MARGIN_TOP = 2 / REFERENCE_HEIGHT;
	private static final float NAVIGATION_INDICATOR_HEIGHT = 10 / REFERENCE_HEIGHT;
	private static final float NAVIGATION_INDICATOR_THICKNESS = 4 / REFERENCE_HEIGHT;
	private static final float INPUT_FIELD_LINE_THICKNESS = 2 / REFERENCE_HEIGHT;
	private static final float Y_DIMENSION_LIST_HEIGHT = 56 / REFERENCE_HEIGHT;
	private static final float X_DIMENSION_LIST_MARGIN_TOP = 8 / REFERENCE_HEIGHT;
	private static final float HORIZONTAL_ARROW_THICKNESS = 4 / REFERENCE_HEIGHT;
	private static final float VERTICAL_ARROW_DOT_LENGTH = 9 / REFERENCE_HEIGHT;
	private static final float VERTICAL_ARROW_DOT_SPACING = 6 / REFERENCE_HEIGHT;

	/**
	 * Hide the constructor in order to prevent instantiating a class containing only static members.
	 */
	private StylesheetBuilder() {
		// Prevent instantiation.
	}

	/**
	 * Creates a {@link CascadingStylesheet} and populates it with all the style definitions.
	 *
	 * @param screenWidth
	 *            the width of the screen to use for size computation.
	 * @param screenHeight
	 *            the height of the screen to use for size computation.
	 *
	 * @return the created {@link CascadingStylesheet}.
	 */
	public static CascadingStylesheet build(int screenWidth, int screenHeight) {
		CascadingStylesheet stylesheet = new CascadingStylesheet();

		EditableStyle style = stylesheet.getDefaultStyle();
		style.setBackground(new RectangularBackground(Colors.BLACK));
		style.setHorizontalAlignment(Alignment.HCENTER);
		style.setVerticalAlignment(Alignment.VCENTER);
		style.setColor(Colors.WHITE);
		style.setFont(Fonts.getMediumSourceSansPro300());

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.BLUETOOTH_IMAGE_WIDGET));
		style.setColor(BLUETOOTH_COLOR);

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.BATTERY_IMAGE_WIDGET));
		style.setColor(Colors.WHITE);

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.SAW_IMAGE_WIDGET));
		style.setColor(SECONDARY_COLOR);

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.SELECTED_SAW_LIST));
		int horizontalMarginSelectedSaw = computeSizing(screenWidth, SELECTED_SAW_LIST_MARGIN_HORIZONTAL);
		style.setMargin(new FlexibleOutline(0, horizontalMarginSelectedSaw, 0, horizontalMarginSelectedSaw));

		style = stylesheet.getSelectorStyle(new TypeSelector(StatusHeader.class));
		style.setPadding(new UniformOutline(computeSizing(screenWidth, STATUS_HEADER_PADDING)));

		style = stylesheet.getSelectorStyle(new TypeSelector(NavigationInfo.class));
		int horizontalPaddingNavInfo = computeSizing(screenWidth, NAVIGATION_INFO_PADDING_HORIZONTAL);
		style.setPadding(new FlexibleOutline(0, horizontalPaddingNavInfo, 0, horizontalPaddingNavInfo));
		style.setColor(PRIMARY_COLOR);
		style.setFont(Fonts.getMediumSourceSansPro600());

		style = stylesheet.getSelectorStyle(new TypeSelector(NavigationIndicator.class));
		style.setMargin(new FlexibleOutline(computeSizing(screenHeight, NAVIGATION_INDICATOR_MARGIN_TOP), 0, 0, 0));
		style.setDimension(
				new FixedDimension(Widget.NO_CONSTRAINT, computeSizing(screenHeight, NAVIGATION_INDICATOR_HEIGHT)));
		style.setExtraInt(NavigationIndicator.STYLE_THICKNESS,
				computeSizing(screenHeight, NAVIGATION_INDICATOR_THICKNESS));

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.UNSELECTED_SIDE_ARROW));
		style.setDimension(
				new FixedDimension(computeSizing(screenWidth, UNSELECTED_ARROW_WIDTH), Widget.NO_CONSTRAINT));
		style.setColor(SECONDARY_COLOR);

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.SELECTED_SIDE_ARROW));
		style.setDimension(new FixedDimension(computeSizing(screenWidth, SELECTED_ARROW_WIDTH), Widget.NO_CONSTRAINT));
		style.setColor(PRIMARY_COLOR);

		style = stylesheet.getSelectorStyle(new TypeSelector(InputField.class));
		style.setDimension(OptimalDimension.OPTIMAL_DIMENSION_X);
		style.setColor(PRIMARY_COLOR);
		style.setFont(Fonts.getLargeSourceSansPro600());
		style.setExtraInt(InputField.STYLE_THICKNESS, computeSizing(screenHeight, INPUT_FIELD_LINE_THICKNESS));

		style = stylesheet.getSelectorStyle(new TypeSelector(TypeWidget.class));
		style.setColor(Colors.BLACK);
		style.setFont(Fonts.getMediumSourceSansPro600());

		style = stylesheet.getSelectorStyle(new TypeSelector(DimensionsWidget.class));
		style.setDimension(OptimalDimension.OPTIMAL_DIMENSION_XY);

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.DIMENSION));
		style.setColor(SECONDARY_COLOR);
		style.setFont(Fonts.getMediumSourceSansPro600());

		style = stylesheet.getSelectorStyle(new AndCombinator(new ClassSelector(ClassSelectors.DIMENSION),
				new ClassSelector(ClassSelectors.SELECTED_DIMENSION)));
		style.setColor(PRIMARY_COLOR);

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.Y_DIMENSION_LIST));
		style.setVerticalAlignment(Alignment.TOP);
		style.setDimension(
				new FixedDimension(Widget.NO_CONSTRAINT, computeSizing(screenHeight, Y_DIMENSION_LIST_HEIGHT)));
		style.setMargin(new FlexibleOutline(0, computeSizing(screenWidth, Y_DIMENSION_LIST_MARGIN_RIGHT), 0, 0));

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.Y_DIMENSION_LABEL));
		style.setHorizontalAlignment(Alignment.RIGHT);
		style.setMargin(new FlexibleOutline(0, computeSizing(screenWidth, Y_DIMENSION_LABEL_MARGIN_RIGHT), 0, 0));

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.X_DIMENSION_LIST));
		style.setMargin(new FlexibleOutline(computeSizing(screenHeight, X_DIMENSION_LIST_MARGIN_TOP), 0, 0, 0));

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.X_DIMENSION_LIST_A));
		style.setHorizontalAlignment(Alignment.LEFT);
		style.setDimension(
				new FixedDimension(computeSizing(screenWidth, X_DIMENSION_LIST_A_WIDTH), Widget.NO_CONSTRAINT));

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.X_DIMENSION_LIST_B));
		style.setHorizontalAlignment(Alignment.HCENTER);
		style.setDimension(
				new FixedDimension(computeSizing(screenWidth, X_DIMENSION_LIST_B_WIDTH), Widget.NO_CONSTRAINT));

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.X_DIMENSION_LIST_C));
		style.setHorizontalAlignment(Alignment.RIGHT);
		style.setDimension(
				new FixedDimension(computeSizing(screenWidth, X_DIMENSION_LIST_C_WIDTH), Widget.NO_CONSTRAINT));

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.X_DIMENSION_LABEL));
		style.setHorizontalAlignment(Alignment.HCENTER);

		style = stylesheet.getSelectorStyle(new TypeSelector(HorizontalArrow.class));
		style.setExtraInt(HorizontalArrow.STYLE_THICKNESS, computeSizing(screenHeight, HORIZONTAL_ARROW_THICKNESS));
		style.setExtraInt(HorizontalArrow.STYLE_DOT_LENGTH, computeSizing(screenWidth, HORIZONTAL_ARROW_DOT_LENGTH));
		style.setExtraInt(HorizontalArrow.STYLE_DOT_SPACING, computeSizing(screenWidth, HORIZONTAL_ARROW_DOT_SPACING));

		style = stylesheet.getSelectorStyle(new TypeSelector(VerticalArrow.class));
		style.setExtraInt(VerticalArrow.STYLE_THICKNESS, computeSizing(screenWidth, VERTICAL_ARROW_THICKNESS));
		style.setExtraInt(VerticalArrow.STYLE_DOT_LENGTH, computeSizing(screenHeight, VERTICAL_ARROW_DOT_LENGTH));
		style.setExtraInt(VerticalArrow.STYLE_DOT_SPACING, computeSizing(screenHeight, VERTICAL_ARROW_DOT_SPACING));

		return stylesheet;
	}

	/**
	 * Computes sizing from percentage value.
	 * <p>
	 * The requested percentage value is from <code>0.f</code> to <code>100.f</code>.
	 *
	 * @param contentSize
	 *            the contentSize that is used as reference.
	 * @param requestedDividedSize
	 *            the requested size divided by the reference value.
	 * @return the computed size in pixel (rounded).
	 */
	private static int computeSizing(int contentSize, float requestedDividedSize) {
		return (int) (contentSize * requestedDividedSize + ROUND);
	}
}
