/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.resources;

import ej.microui.display.Font;

/**
 * A static class that gives access to the different fonts of the application.
 */
public class Fonts {

	/**
	 * Hide the constructor in order to prevent instantiating a class containing only static members.
	 */
	private Fonts() {
		// Prevent instantiation.
	}

	/**
	 * Gets the <b>medium</b> font of family SourceSansPro with a weight of
	 * <b>300</b>.
	 *
	 * @return the font.
	 */
	public static Font getMediumSourceSansPro300() {
		return Font.getFont("/fonts/SourceSansPro_Medium-300.ejf"); //$NON-NLS-1$
	}

	/**
	 * Gets the <b>medium</b> font of family SourceSansPro with a weight of
	 * <b>600</b>.
	 *
	 * @return the font.
	 */
	public static Font getMediumSourceSansPro600() {
		return Font.getFont("/fonts/SourceSansPro_Medium-600.ejf"); //$NON-NLS-1$
	}

	/**
	 * Gets the <b>large</b> font of family SourceSansPro with a weight of
	 * <b>600</b>.
	 *
	 * @return the font.
	 */
	public static Font getLargeSourceSansPro600() {
		return Font.getFont("/fonts/SourceSansPro_Large-600.ejf"); //$NON-NLS-1$
	}
}
