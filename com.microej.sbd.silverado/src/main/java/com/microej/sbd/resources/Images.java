/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.resources;

/**
 * A static class containing the path to all the images used throughout the application.
 */
public class Images {

	/** Logo image (big version). */
	public static final String LOGO_BIG = "/images/logo_big.png"; //$NON-NLS-1$
	/** Logo image (small version). */
	public static final String LOGO_SMALL = "/images/logo_small.png"; //$NON-NLS-1$

	/** Image for the saw icon. */
	public static final String SAW = "/images/saw.png"; //$NON-NLS-1$

	/** Arrow head left icon. */
	public static final String LEFT_ARROW = "/images/arrow_left.png"; //$NON-NLS-1$
	/** Arrow head right icon. */
	public static final String RIGHT_ARROW = "/images/arrow_right.png"; //$NON-NLS-1$
	/** Arrow head up icon. */
	public static final String UP_ARROW = "/images/arrow_up.png"; //$NON-NLS-1$
	/** Arrow head down icon. */
	public static final String DOWN_ARROW = "/images/arrow_down.png"; //$NON-NLS-1$

	/** Image for Bluetooth icon in header. */
	public static final String BLUETOOTH = "/images/bluetooth.png"; //$NON-NLS-1$
	/** Image for battery icon in header. */
	public static final String BATTERY = "/images/battery.png"; //$NON-NLS-1$

	/** Image for cutting edge type A. */
	public static final String TYPE_A = "/images/type_a.png"; //$NON-NLS-1$
	/** Image for cutting edge type B. */
	public static final String TYPE_B = "/images/type_b.png"; //$NON-NLS-1$
	/** Image for cutting edge type C. */
	public static final String TYPE_C = "/images/type_c.png"; //$NON-NLS-1$

	/**
	 * Hide the constructor in order to prevent instantiating a class containing only static members.
	 */
	private Images() {
		// Prevent instantiation.
	}
}
