/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.resources;

/**
 * A static class where the class selectors for all the widgets used throughout the application. Used primarily to link
 * widgets to their corresponding style.
 */
public class ClassSelectors {

	/** Class selector for the Bluetooth icon. */
	public static final int BLUETOOTH_IMAGE_WIDGET = 1;
	/** Class selector for the battery icon. */
	public static final int BATTERY_IMAGE_WIDGET = 2;

	/** Class selector for the saw icon. */
	public static final int SAW_IMAGE_WIDGET = 3;
	/** Class selector for the side arrow. */
	public static final int UNSELECTED_SIDE_ARROW = 4;
	/** Class selector for the side arrow when selected. */
	public static final int SELECTED_SIDE_ARROW = 5;
	/** Class selector for the selected saw side list. */
	public static final int SELECTED_SAW_LIST = 6;

	/** Class selector for the dimension label. */
	public static final int DIMENSION = 7;
	/** Class selector for the dimension label when selected. */
	public static final int SELECTED_DIMENSION = 8;
	/** Class selector for a list containing the Y dimension widgets. */
	public static final int Y_DIMENSION_LIST = 9;
	/** Class selector for the Y dimension label. */
	public static final int Y_DIMENSION_LABEL = 10;
	/** Class selector for a list containing the X dimension widgets. */
	public static final int X_DIMENSION_LIST = 11;
	/** Class selector for a list containing the X dimension widgets for type A cut. */
	public static final int X_DIMENSION_LIST_A = 12;
	/** Class selector for a list containing the X dimension widgets for type B cut. */
	public static final int X_DIMENSION_LIST_B = 13;
	/** Class selector for a list containing the X dimension widgets for type C cut. */
	public static final int X_DIMENSION_LIST_C = 14;
	/** Class selector for the X dimension label. */
	public static final int X_DIMENSION_LABEL = 15;

	/*
	 * Class IDs 1000 - 1010 are reserved for virtual button styles.
	 */
	/** Class selector for container of the content when using virtual buttons. */
	public static final int VIRTUAL_CONTENT_CONTAINER = 1000;
	/** Class selector for container of the virtual buttons. */
	public static final int VIRTUAL_BUTTON_CONTAINER = 1001;
	/** Class selector for virtual buttons. */
	public static final int VIRTUAL_BUTTON = 1002;
	/** Class selector for the "context" virtual buttons in the first row. */
	public static final int VIRTUAL_BUTTON_CONTEXT = 1003;
	/** Class selector for the "special" virtual buttons in the last row. */
	public static final int VIRTUAL_BUTTON_SPECIAL = 1004;
	/** Class selector for grid of the virtual buttons. */
	public static final int VIRTUAL_BUTTON_GRID = 1005;

	private ClassSelectors() {
		// private constructor
	}
}
