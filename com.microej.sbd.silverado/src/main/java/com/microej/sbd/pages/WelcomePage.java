/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.pages;

import com.microej.sbd.Main;
import com.microej.sbd.resources.Images;

import ej.bon.Timer;
import ej.bon.TimerTask;
import ej.service.ServiceFactory;
import ej.widget.basic.ImageWidget;

/**
 * Initial welcome page displaying the logo for a short time.
 */
public class WelcomePage extends ImageWidget {

	private static final long DELAY = 4_000;

	/**
	 * Creates the WelcomePage.
	 */
	public WelcomePage() {
		super(Images.LOGO_BIG);
	}

	@Override
	protected void onShown() {
		super.onShown();

		Timer timer = ServiceFactory.getService(Timer.class, Timer.class);
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				handleDelayPassed();
			}
		}, DELAY);
	}

	private void handleDelayPassed() {
		Main.showPage(new HomePage());
	}
}
