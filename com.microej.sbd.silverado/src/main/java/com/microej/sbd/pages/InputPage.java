/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.pages;

import com.microej.sbd.Main;
import com.microej.sbd.framework.ButtonCommands;
import com.microej.sbd.widgets.DimensionInputField;

import ej.widget.basic.Label;
import ej.widget.container.LayoutOrientation;
import ej.widget.container.List;

/**
 * Page to input the dimension.
 */
public class InputPage extends CommonPage {

	private static final int DEFAULT_INT_VALUE = 56;
	private static final int DEFAULT_DEC_VALUE = 3;
	private static final int DEFAULT_UNIT_INDEX = DimensionInputField.UNIT_INDEX_IN;

	private static final int MAX_DIGITS = 4;

	private final DimensionInputField inputField;

	/**
	 * Creates the InputPage.
	 */
	public InputPage() {
		getNavigationFooter().setInfo("Unit", "Angle", "+ / -", "_"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

		this.inputField = new DimensionInputField(MAX_DIGITS, DEFAULT_INT_VALUE, DEFAULT_DEC_VALUE, DEFAULT_UNIT_INDEX);

		List mainList = new List(LayoutOrientation.VERTICAL);
		mainList.addChild(new Label("Insert dimension")); //$NON-NLS-1$
		mainList.addChild(this.inputField);

		super.setCenterChild(mainList);
	}

	@Override
	public boolean handleCommands(int command) {
		if (command == ButtonCommands.CONTEXT_ONE) {
			Main.showPage(new AnglePage());
			return true;
		} else if (this.inputField.handleButtonPress(command)) {
			return true;
		}

		return super.handleCommands(command);
	}
}
