/*
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.pages;

import com.microej.sbd.Main;
import com.microej.sbd.framework.ButtonCommands;
import com.microej.sbd.widgets.NavigationFooter;
import com.microej.sbd.widgets.StatusHeader;

import ej.widget.container.LayoutOrientation;
import ej.widget.container.SimpleDock;

/**
 * Common page base skeleton containing the {@link StatusHeader} and {@link NavigationFooter}.
 */
public class CommonPage extends SimpleDock {

	private final NavigationFooter navigationFooter;

	/**
	 * Creates the CommonPage.
	 */
	public CommonPage() {
		super(LayoutOrientation.VERTICAL);

		this.navigationFooter = new NavigationFooter();

		super.setFirstChild(new StatusHeader());
		super.setLastChild(this.navigationFooter);
	}

	/**
	 * Gets the current NavigationFooter.
	 *
	 * @return the NavigationFooter.
	 */
	protected final NavigationFooter getNavigationFooter() {
		return this.navigationFooter;
	}

	/**
	 * Handles button command send to the page.
	 *
	 * @param command
	 *            the command to handle.
	 *
	 * @return <code>true</code> if the widget has consumed the event, <code>false</code> otherwise.
	 */
	public boolean handleCommands(int command) {
		if (command == ButtonCommands.HOME && getClass() != HomePage.class) {
			Main.showPage(new HomePage());
			return true;
		}

		return false;
	}
}
