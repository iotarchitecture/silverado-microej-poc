/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.pages;

import com.microej.sbd.framework.ButtonCommands;
import com.microej.sbd.widgets.DimensionsWidget;
import com.microej.sbd.widgets.LengthInputField;
import com.microej.sbd.widgets.TypeWidget;

import ej.widget.container.LayoutOrientation;
import ej.widget.container.List;

/**
 * Page to edit sizes of the angle to cut.
 */
public class AngleEditPage extends CommonPage {

	private static final int DEFAULT_X_VALUE = 150;
	private static final int DEFAULT_Y_VALUE = 4;

	private static final int MAX_DIGITS = 3;

	private final DimensionsWidget dimensionsWidget;
	private final LengthInputField inputField;

	/**
	 * Creates the AngleEditPage.
	 *
	 * @param type
	 *            the angle type used for {@link TypeWidget}.
	 */
	public AngleEditPage(int type) {
		getNavigationFooter().setInfo("<", ">", "Save", "Edit"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

		this.dimensionsWidget = new DimensionsWidget(type, DimensionsWidget.DIMENSION_X, DEFAULT_X_VALUE,
				DEFAULT_Y_VALUE, MAX_DIGITS);

		this.inputField = new LengthInputField(MAX_DIGITS, DEFAULT_X_VALUE);

		List mainList = new List(LayoutOrientation.VERTICAL);
		mainList.addChild(this.dimensionsWidget);
		mainList.addChild(this.inputField);

		super.setCenterChild(mainList);
	}

	@Override
	public boolean handleCommands(int command) {
		if (command == ButtonCommands.CONTEXT_ZERO) {
			changeSelectedDimension(1);
			return true;
		} else if (command == ButtonCommands.CONTEXT_ONE) {
			changeSelectedDimension(-1);
			return true;
		} else if (command == ButtonCommands.CONTEXT_TWO) {
			saveDimensionValue();
			return true;
		} else {
			this.inputField.handleButtonPress(command);
		}

		return super.handleCommands(command);
	}

	private void changeSelectedDimension(int increment) {
		int oldSelectedDimension = this.dimensionsWidget.getSelectedDimension();
		int newSelectedDimension = (oldSelectedDimension + increment + DimensionsWidget.NUM_DIMENSIONS)
				% DimensionsWidget.NUM_DIMENSIONS;
		this.dimensionsWidget.setSelectedDimension(newSelectedDimension);
		this.dimensionsWidget.requestRender();

		this.inputField.setValue(this.dimensionsWidget.getDimensionValue(newSelectedDimension));
		this.inputField.requestRender();
	}

	private void saveDimensionValue() {
		int selectedDimension = this.dimensionsWidget.getSelectedDimension();
		int value = this.inputField.getValue();
		this.dimensionsWidget.setDimensionValue(selectedDimension, value);
		this.dimensionsWidget.requestRender();
	}
}
