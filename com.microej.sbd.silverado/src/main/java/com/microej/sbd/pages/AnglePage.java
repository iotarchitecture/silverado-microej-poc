/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.pages;

import com.microej.sbd.Main;
import com.microej.sbd.framework.ButtonCommands;
import com.microej.sbd.widgets.TypeWidget;

/**
 * Page to select the angle to cut.
 */
public class AnglePage extends CommonPage {

	private final TypeWidget typeWidget;

	/**
	 * Creates the AnglePage.
	 */
	public AnglePage() {
		getNavigationFooter().setInfo("v", "^", "Save", "Edit"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

		this.typeWidget = new TypeWidget(TypeWidget.TYPE_A);

		super.setCenterChild(this.typeWidget);
	}

	@Override
	public boolean handleCommands(int command) {
		if (command == ButtonCommands.CONTEXT_ZERO) {
			changeType(1);
			return true;
		} else if (command == ButtonCommands.CONTEXT_ONE) {
			changeType(-1);
			return true;
		} else if (command == ButtonCommands.CONTEXT_THREE) {
			Main.showPage(new AngleEditPage(this.typeWidget.getType()));
			return true;
		}

		return super.handleCommands(command);
	}

	private void changeType(int increment) {
		int oldType = this.typeWidget.getType();
		this.typeWidget.setType((oldType + increment + TypeWidget.NUM_TYPES) % TypeWidget.NUM_TYPES);
		this.typeWidget.requestRender();
	}
}
