/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.pages;

import com.microej.sbd.Main;
import com.microej.sbd.framework.ButtonCommands;
import com.microej.sbd.resources.ClassSelectors;
import com.microej.sbd.resources.Images;
import com.microej.sbd.widgets.HorizontalArrow;

import ej.widget.basic.ImageWidget;
import ej.widget.container.LayoutOrientation;
import ej.widget.container.List;

/**
 * HomePage after choosing a side.
 */
public class SideHomePage extends CommonPage {

	/**
	 * Creates the SideHomePage.
	 *
	 * @param isLeftSide
	 *            <code>true</code> if the left side is used or <code>false</code> for the right side.
	 */
	public SideHomePage(boolean isLeftSide) {
		getNavigationFooter().setInfo("Input", "Lists", "Set", "BT"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

		ImageWidget sawImageWidget = new ImageWidget(Images.SAW);
		sawImageWidget.addClassSelector(ClassSelectors.SAW_IMAGE_WIDGET);

		HorizontalArrow arrowsWidget = new HorizontalArrow();
		arrowsWidget.addClassSelector(ClassSelectors.SELECTED_SIDE_ARROW);

		List sawList = new List(LayoutOrientation.HORIZONTAL);
		sawList.addClassSelector(ClassSelectors.SELECTED_SAW_LIST);
		if (isLeftSide) {
			sawList.addChild(sawImageWidget);
			sawList.addChild(arrowsWidget);
		} else {
			sawList.addChild(arrowsWidget);
			sawList.addChild(sawImageWidget);
		}

		super.setCenterChild(sawList);
	}

	@Override
	public boolean handleCommands(int command) {
		if (command == ButtonCommands.CONTEXT_ZERO) {
			Main.showPage(new InputPage());
			return true;
		}

		return super.handleCommands(command);
	}
}
