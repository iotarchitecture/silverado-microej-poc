/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.pages;

import com.microej.sbd.Main;
import com.microej.sbd.framework.ButtonCommands;
import com.microej.sbd.resources.ClassSelectors;
import com.microej.sbd.resources.Images;
import com.microej.sbd.widgets.HorizontalArrow;
import com.microej.sbd.widgets.NavigationFooter;

import ej.widget.basic.ImageWidget;
import ej.widget.basic.Label;
import ej.widget.container.LayoutOrientation;
import ej.widget.container.List;

/**
 * HomePage that is directly displayed after welcome.
 */
public class HomePage extends CommonPage {

	/**
	 * Creates the HomePage.
	 */
	public HomePage() {
		NavigationFooter navigationFooter = getNavigationFooter();
		navigationFooter.setInfoLeft("Left"); //$NON-NLS-1$
		navigationFooter.setInfoRight("Right"); //$NON-NLS-1$

		ImageWidget sawImageWidget = new ImageWidget(Images.SAW);
		sawImageWidget.addClassSelector(ClassSelectors.SAW_IMAGE_WIDGET);

		HorizontalArrow leftArrowWidget = new HorizontalArrow();
		leftArrowWidget.addClassSelector(ClassSelectors.UNSELECTED_SIDE_ARROW);

		HorizontalArrow rightArrowWidget = new HorizontalArrow();
		rightArrowWidget.addClassSelector(ClassSelectors.UNSELECTED_SIDE_ARROW);

		List sawList = new List(LayoutOrientation.HORIZONTAL);
		sawList.addChild(leftArrowWidget);
		sawList.addChild(sawImageWidget);
		sawList.addChild(rightArrowWidget);

		List mainList = new List(LayoutOrientation.VERTICAL);
		mainList.addChild(new Label("Select Work Side")); //$NON-NLS-1$
		mainList.addChild(sawList);

		super.setCenterChild(mainList);
	}

	@Override
	public boolean handleCommands(int command) {
		if (command == ButtonCommands.CONTEXT_ZERO) {
			Main.showPage(new SideHomePage(true));
			return true;
		} else if (command == ButtonCommands.CONTEXT_THREE) {
			Main.showPage(new SideHomePage(false));
			return true;
		}

		return super.handleCommands(command);
	}
}
