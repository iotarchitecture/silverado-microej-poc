# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2021-09-28

### Added

- Initial commit.

---
_Copyright 2021 MicroEJ Corp. All rights reserved._  
_This Software has been designed or modified by MicroEJ Corp._  
_MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software._  
