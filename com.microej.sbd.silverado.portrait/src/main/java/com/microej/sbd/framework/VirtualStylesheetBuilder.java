/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.framework;

import com.microej.sbd.resources.ClassSelectors;

import ej.mwt.style.EditableStyle;
import ej.mwt.style.background.RectangularBackground;
import ej.mwt.style.background.RoundedBackground;
import ej.mwt.style.dimension.FixedDimension;
import ej.mwt.style.outline.FlexibleOutline;
import ej.mwt.style.outline.NoOutline;
import ej.mwt.style.outline.border.FlexibleRectangularBorder;
import ej.mwt.style.outline.border.RoundedBorder;
import ej.mwt.stylesheet.cascading.CascadingStylesheet;
import ej.mwt.stylesheet.selector.ClassSelector;
import ej.mwt.stylesheet.selector.StateSelector;
import ej.mwt.stylesheet.selector.combinator.AndCombinator;
import ej.widget.basic.Button;

/**
 * Builds the stylesheet for the virtual buttons.
 */
public class VirtualStylesheetBuilder {

	private static final int COLOR_BACKGROUND = 0x272627;
	private static final int COLOR_BUTTON_BACKGROUND = 0x403e40;
	private static final int COLOR_BUTTON_TEXT = 0xf5ba10;
	private static final int COLOR_BUTTON_BACKGROUND_SPECIAL = 0x5a595b;
	private static final int COLOR_BUTTON_BORDER = 0x707070;
	private static final int COLOR_BUTTON_BORDER_CONTEXT = 0xf5ba10;

	// All sizes used here are pixel not percentage, since only the application scales.
	private static final int BUTTON_WIDTH = 67;
	private static final int BUTTON_HEIGHT = 48;

	private static final int BUTTON_RADIUS = 7;
	private static final int BUTTON_BORDER_THICKNESS = 1;
	private static final int BUTTON_BORDER_THICKNESS_CONTEXT = 1;
	private static final int BUTTON_BORDER_THICKNESS_CONTEXT_ACTIVE = 3;

	private static final int TOP_ROW_MARGIN = 40;
	private static final int BOTTOM_ROW_MARGIN = 20;

	/**
	 * Hide the constructor in order to prevent instantiating a class containing only static members.
	 */
	private VirtualStylesheetBuilder() {
		// Prevent instantiation.
	}

	/**
	 * Appends an existing {@link CascadingStylesheet} with the styles required for the virtual buttons.
	 *
	 * @param stylesheet
	 *            the {@link CascadingStylesheet} to append.
	 */
	public static void addVirtualFrontpanelStyles(CascadingStylesheet stylesheet) {
		StateSelector buttonPressed = new StateSelector(Button.ACTIVE);

		EditableStyle style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.VIRTUAL_CONTENT_CONTAINER));
		style.setDimension(new FixedDimension(ScreenSize.getWidth(), ScreenSize.getHeight()));

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.VIRTUAL_BUTTON_CONTAINER));
		style.setBackground(new RectangularBackground(COLOR_BACKGROUND));
		style.setBorder(new FlexibleRectangularBorder(COLOR_BUTTON_BACKGROUND, 1, 0, 0, 0));

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.VIRTUAL_BUTTON_GRID));
		style.setBackground(new RectangularBackground(COLOR_BACKGROUND));

		ClassSelector buttonSelector = new ClassSelector(ClassSelectors.VIRTUAL_BUTTON);
		style = stylesheet.getSelectorStyle(buttonSelector);
		style.setDimension(new FixedDimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		style.setColor(COLOR_BUTTON_TEXT);
		style.setBackground(new RoundedBackground(COLOR_BUTTON_BACKGROUND, BUTTON_RADIUS));
		style.setBorder(new RoundedBorder(COLOR_BUTTON_BORDER, BUTTON_RADIUS, BUTTON_BORDER_THICKNESS));

		style = stylesheet.getSelectorStyle(new ClassSelector(ClassSelectors.VIRTUAL_BUTTON_SPECIAL));
		style.setBackground(new RoundedBackground(COLOR_BUTTON_BACKGROUND_SPECIAL, BUTTON_RADIUS));
		style.setMargin(new FlexibleOutline(BOTTOM_ROW_MARGIN, 0, 0, 0));

		ClassSelector contextSelector = new ClassSelector(ClassSelectors.VIRTUAL_BUTTON_CONTEXT);
		style = stylesheet.getSelectorStyle(contextSelector);
		style.setBorder(new RoundedBorder(COLOR_BUTTON_BORDER_CONTEXT, BUTTON_RADIUS, BUTTON_BORDER_THICKNESS_CONTEXT));
		style.setMargin(new FlexibleOutline(0, 0, TOP_ROW_MARGIN, 0));

		style = stylesheet.getSelectorStyle(new AndCombinator(buttonSelector, buttonPressed));
		style.setBorder(NoOutline.NO_OUTLINE);

		style = stylesheet.getSelectorStyle(new AndCombinator(contextSelector, buttonPressed));
		style.setBorder(
				new RoundedBorder(COLOR_BUTTON_BORDER_CONTEXT, BUTTON_RADIUS, BUTTON_BORDER_THICKNESS_CONTEXT_ACTIVE));
	}
}
