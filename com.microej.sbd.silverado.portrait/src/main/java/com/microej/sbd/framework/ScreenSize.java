/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.framework;

/**
 * The screen sizes to use.
 */
public class ScreenSize {
	private static final int WIDTH = 272;
	private static final int HEIGHT = 154;

	/**
	 * Hide the constructor in order to prevent instantiating a class containing only static members.
	 */
	private ScreenSize() {
		// Prevent instantiation.
	}

	/**
	 * Gets the width of the application screen.
	 *
	 * @return the width of the application screen.
	 */
	public static int getWidth() {
		return WIDTH;
	}

	/**
	 * Gets the height of the application screen.
	 *
	 * @return the height of the application screen.
	 */
	public static int getHeight() {
		return HEIGHT;
	}
}
