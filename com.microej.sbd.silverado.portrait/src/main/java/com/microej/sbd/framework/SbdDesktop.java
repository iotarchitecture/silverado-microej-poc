/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.framework;

import com.microej.sbd.pages.CommonPage;
import com.microej.sbd.resources.ClassSelectors;

import ej.annotation.Nullable;
import ej.microui.event.Event;
import ej.microui.event.EventGenerator;
import ej.microui.event.generator.Command;
import ej.mwt.Desktop;
import ej.mwt.Widget;
import ej.mwt.render.OverlapRenderPolicy;
import ej.mwt.render.RenderPolicy;
import ej.mwt.stylesheet.CachedStylesheet;
import ej.mwt.stylesheet.Stylesheet;
import ej.mwt.stylesheet.cascading.CascadingStylesheet;
import ej.widget.basic.Button;
import ej.widget.basic.OnClickListener;
import ej.widget.container.Grid;
import ej.widget.container.LayoutOrientation;
import ej.widget.container.List;
import ej.widget.container.SimpleDock;

/**
 * Desktop implementation for the SBD Silverado project.
 */
public class SbdDesktop extends Desktop {

	private static final int BUTTONS_PER_ROW = 4;

	// widget shown on the desktop
	@Nullable
	private Widget content;

	/**
	 * Creates SbdDesktop.
	 */
	public SbdDesktop() {
		super.setWidget(buildVirtualButtonContainer());
	}

	@Override
	protected RenderPolicy createRenderPolicy() {
		return new OverlapRenderPolicy(this);
	}

	/**
	 * Sets the stylesheet instance as a {@link CachedStylesheet}.
	 *
	 * @param stylesheet
	 *            the stylesheet instance.
	 */
	@Override
	public void setStylesheet(Stylesheet stylesheet) {
		if (stylesheet instanceof CascadingStylesheet) {
			VirtualStylesheetBuilder.addVirtualFrontpanelStyles((CascadingStylesheet) stylesheet);
			super.setStylesheet(new CachedStylesheet(stylesheet));
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Sets the stylesheet instance as a {@link CachedStylesheet}.
	 *
	 * @param stylesheet
	 *            the stylesheet instance.
	 *
	 * @deprecated Not for use. Use setStylesheet with a {@link CascadingStylesheet} instead.
	 */
	@Deprecated
	public void setStylesheet(CachedStylesheet stylesheet) {
		throw new IllegalArgumentException("Use setStylesheet with a CascadingStylesheet."); //$NON-NLS-1$
	}

	/**
	 * Gets the content widget attached to this desktop <b>without</b> virtual buttons..
	 *
	 * @return the content widget attached with this desktop or <code>null</code>.
	 */
	@Nullable
	public Widget getContentWidget() {
		return this.content;
	}

	@Override
	public void setWidget(@Nullable Widget content) {
		SimpleDock container = (SimpleDock) getWidget();
		assert (container != null);

		if (content != null) {
			content.addClassSelector(ClassSelectors.VIRTUAL_CONTENT_CONTAINER);
			container.setFirstChild(content);
		} else {
			Widget currentContent = container.getFirstChild();
			if (currentContent != null) {
				container.removeChild(currentContent);
			}
		}
		this.content = content;
	}

	private SimpleDock buildVirtualButtonContainer() {
		SimpleDock container = new SimpleDock(LayoutOrientation.VERTICAL);
		List gridContainer = new List(LayoutOrientation.VERTICAL);
		gridContainer.addClassSelector(ClassSelectors.VIRTUAL_BUTTON_CONTAINER);

		Grid contextGrid = new Grid(LayoutOrientation.HORIZONTAL, BUTTONS_PER_ROW);
		// First Row
		contextGrid.addChild(getContextButton(ButtonCommands.CONTEXT_ZERO));
		contextGrid.addChild(getContextButton(ButtonCommands.CONTEXT_ONE));
		contextGrid.addChild(getContextButton(ButtonCommands.CONTEXT_TWO));
		contextGrid.addChild(getContextButton(ButtonCommands.CONTEXT_THREE));

		Grid grid = new Grid(LayoutOrientation.HORIZONTAL, BUTTONS_PER_ROW);
		// Second Row
		grid.addChild(getNumpadButton("6", ButtonCommands.DIGIT_SIX)); //$NON-NLS-1$
		grid.addChild(getNumpadButton("7", ButtonCommands.DIGIT_SEVEN)); //$NON-NLS-1$
		grid.addChild(getNumpadButton("8", ButtonCommands.DIGIT_EIGHT)); //$NON-NLS-1$
		grid.addChild(getNumpadButton("9", ButtonCommands.DIGIT_NINE)); //$NON-NLS-1$

		// Third Row
		grid.addChild(getNumpadButton("2", ButtonCommands.DIGIT_TWO)); //$NON-NLS-1$
		grid.addChild(getNumpadButton("3", ButtonCommands.DIGIT_THREE)); //$NON-NLS-1$
		grid.addChild(getNumpadButton("4", ButtonCommands.DIGIT_FOUR)); //$NON-NLS-1$
		grid.addChild(getNumpadButton("5", ButtonCommands.DIGIT_FIVE)); //$NON-NLS-1$

		// Fourth Row
		grid.addChild(getNumpadButton("1", ButtonCommands.DIGIT_ONE)); //$NON-NLS-1$
		grid.addChild(getNumpadButton("0", ButtonCommands.DIGIT_ZERO)); //$NON-NLS-1$
		grid.addChild(getNumpadButton("-/", ButtonCommands.SLASH)); //$NON-NLS-1$
		grid.addChild(getNumpadButton("Clr", ButtonCommands.CLEAR)); //$NON-NLS-1$

		Grid specialGrid = new Grid(LayoutOrientation.HORIZONTAL, BUTTONS_PER_ROW);
		// Fifth / Last Row
		specialGrid.addChild(getSpecialButton("Home", ButtonCommands.HOME)); //$NON-NLS-1$
		specialGrid.addChild(getSpecialButton("Fix", ButtonCommands.FIX)); //$NON-NLS-1$
		specialGrid.addChild(getSpecialButton("Reset", ButtonCommands.RESET)); //$NON-NLS-1$
		specialGrid.addChild(getSpecialButton("Enter", ButtonCommands.ENTER)); //$NON-NLS-1$

		contextGrid.addClassSelector(ClassSelectors.VIRTUAL_BUTTON_GRID);
		grid.addClassSelector(ClassSelectors.VIRTUAL_BUTTON_GRID);
		specialGrid.addClassSelector(ClassSelectors.VIRTUAL_BUTTON_GRID);
		gridContainer.addChild(contextGrid);
		gridContainer.addChild(grid);
		gridContainer.addChild(specialGrid);

		container.setCenterChild(gridContainer);
		return container;
	}

	private Button getNumpadButton(String text, final int command) {
		Button b = new Button(text);
		b.addClassSelector(ClassSelectors.VIRTUAL_BUTTON);
		b.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				sendCommand(command);
			}
		});
		return b;
	}

	private Button getContextButton(int command) {
		Button b = getNumpadButton("", command); //$NON-NLS-1$
		b.addClassSelector(ClassSelectors.VIRTUAL_BUTTON_CONTEXT);
		return b;
	}

	private Button getSpecialButton(String text, int command) {
		Button b = getNumpadButton(text, command);
		b.addClassSelector(ClassSelectors.VIRTUAL_BUTTON_SPECIAL);
		return b;
	}

	@Override
	public boolean handleEvent(int event) {
		if (Event.getType(event) == Command.EVENT_TYPE) {
			Widget widget = getContentWidget();
			if (widget instanceof CommonPage) {
				return ((CommonPage) widget).handleCommands(Event.getData(event));
			}
			return false;
		}
		return super.handleEvent(event);
	}

	/**
	 * Sends a command through the event handler.
	 *
	 * @param command
	 *            the command to send as int.
	 *
	 * @see Command
	 * @see ButtonCommands
	 */
	public static void sendCommand(int command) {
		Command commands = EventGenerator.get(Command.class, 0);
		if (commands != null) {
			commands.send(command);
		}
	}
}
