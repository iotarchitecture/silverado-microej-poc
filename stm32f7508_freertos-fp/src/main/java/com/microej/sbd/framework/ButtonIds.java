/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.framework;

public class ButtonIds {

	/** ID for "Context 0" Button. */
	public static final int CONTEXT_0 = 0;
	/** ID for "Context 1" Button. */
	public static final int CONTEXT_1 = 1;
	/** ID for "Context 2" Button. */
	public static final int CONTEXT_2 = 2;
	/** ID for "Context 3" Button. */
	public static final int CONTEXT_3 = 3;
	/** ID for "6" Button. */
	public static final int DIGIT_6 = 4;
	/** ID for "7" Button. */
	public static final int DIGIT_7 = 5;
	/** ID for "8" Button. */
	public static final int DIGIT_8 = 6;
	/** ID for "9" Button. */
	public static final int DIGIT_9 = 7;
	/** ID for "2" Button. */
	public static final int DIGIT_2 = 8;
	/** ID for "3" Button. */
	public static final int DIGIT_3 = 9;
	/** ID for "4" Button. */
	public static final int DIGIT_4 = 10;
	/** ID for "5" Button. */
	public static final int DIGIT_5 = 11;
	/** ID for "1" Button. */
	public static final int DIGIT_1 = 12;
	/** ID for "0" Button. */
	public static final int DIGIT_0 = 13;
	/** ID for "-/" Button. */
	public static final int SLASH = 14;
	/** ID for "Clr" Button. */
	public static final int CLEAR = 15;
	/** ID for "Home" Button. */
	public static final int HOME = 16;
	/** ID for "Fix" Button. */
	public static final int FIX = 17;
	/** ID for "Reset" Button. */
	public static final int RESET = 18;
	/** ID for "Enter" Button. */
	public static final int ENTER = 19;

	/** Lowest Button ID. */
	public static final int MIN_ID = CONTEXT_0;
	/** Highest Button ID. */
	public static final int MAX_ID = ENTER;

	private ButtonIds() {
		// private constructor
	}
}
