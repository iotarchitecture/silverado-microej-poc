/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.framework;

/**
 * Class that contains various constants for the Buttons used in the project.
 * <p>
 * Will be shared between FrontPanel project and the application project.
 * <p>
 * The Commands will start at 0x0020 since the constants in the Command event generator go up to 0x0016.
 */
public class ButtonCommands {

	/*
	 * Context Buttons.
	 */

	/**
	 * Constant for "Context 0" command.
	 */
	public static final int CONTEXT_ZERO = 0x0020;
	/**
	 * Constant for "Context 1" command.
	 */
	public static final int CONTEXT_ONE = 0x0021;
	/**
	 * Constant for "Context 2" command.
	 */
	public static final int CONTEXT_TWO = 0x0022;
	/**
	 * Constant for "Context 3" command.
	 */
	public static final int CONTEXT_THREE = 0x0023;

	/*
	 * Special Buttons.
	 */

	/**
	 * Constant for "SLASH" command.
	 */
	public static final int SLASH = 0x0024;

	/**
	 * Constant for "ENTER" command.
	 */
	public static final int ENTER = 0x0025;

	/**
	 * Constant for "HOME" command.
	 */
	public static final int HOME = 0x0026;

	/**
	 * Constant for "FIX" command.
	 */
	public static final int FIX = 0x0027;

	/**
	 * Constant for "CLEAR" command.
	 */
	public static final int CLEAR = 0x0028;

	/**
	 * Constant for "RESET" command.
	 */
	public static final int RESET = 0x0029;

	/*
	 * Digit Buttons. (Starts at 0x0030 to fit with the numbers.)
	 */

	/**
	 * Constant for "0" command.
	 */
	public static final int DIGIT_ZERO = 0x0030;
	/**
	 * Constant for "1" command.
	 */
	public static final int DIGIT_ONE = 0x0031;
	/**
	 * Constant for "2" command.
	 */
	public static final int DIGIT_TWO = 0x0032;
	/**
	 * Constant for "3" command.
	 */
	public static final int DIGIT_THREE = 0x0033;
	/**
	 * Constant for "4" command.
	 */
	public static final int DIGIT_FOUR = 0x0034;
	/**
	 * Constant for "5" command.
	 */
	public static final int DIGIT_FIVE = 0x0035;
	/**
	 * Constant for "6" command.
	 */
	public static final int DIGIT_SIX = 0x0036;
	/**
	 * Constant for "7" command.
	 */
	public static final int DIGIT_SEVEN = 0x0037;
	/**
	 * Constant for "8" command.
	 */
	public static final int DIGIT_EIGHT = 0x0038;
	/**
	 * Constant for "9" command.
	 */
	public static final int DIGIT_NINE = 0x0039;

	private ButtonCommands() {
		// private constructor
	}

	/**
	 * Gets the integer from a digit command.
	 *
	 * @param command
	 *            the command to check.
	 * 
	 * @return the integer for the digit command.
	 */
	public static int getDigitFromCommand(int command) {
		if (command < DIGIT_ZERO || command > DIGIT_NINE) {
			throw new IllegalArgumentException();
		}
		return command - DIGIT_ZERO;
	}
}
