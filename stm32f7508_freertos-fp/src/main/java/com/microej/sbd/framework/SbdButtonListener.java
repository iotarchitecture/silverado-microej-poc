/*
 * Java
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This Software has been designed or modified by MicroEJ Corp.
 * MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
 */
package com.microej.sbd.framework;

import ej.fp.widget.Button;
import ej.microui.event.EventButton;
import ej.microui.event.EventCommand;

/**
 * Listener for Button actions of the SBD FrontPanel.
 */
public class SbdButtonListener implements ej.fp.widget.Button.ButtonListener {

	@Override
	public void press(Button button) {
		int cmd;
		switch (button.getID()) {
		// Context Buttons.
		case ButtonIds.CONTEXT_0: cmd = ButtonCommands.CONTEXT_ZERO; break;
		case ButtonIds.CONTEXT_1: cmd = ButtonCommands.CONTEXT_ONE; break;
		case ButtonIds.CONTEXT_2: cmd = ButtonCommands.CONTEXT_TWO; break;
		case ButtonIds.CONTEXT_3: cmd = ButtonCommands.CONTEXT_THREE; break;
		// Special Buttons.
		case ButtonIds.SLASH: 	cmd = ButtonCommands.SLASH; break;
		case ButtonIds.ENTER:	cmd = ButtonCommands.ENTER; break;
		case ButtonIds.HOME: 	cmd = ButtonCommands.HOME; break;
		case ButtonIds.FIX: 	cmd = ButtonCommands.FIX; break;
		case ButtonIds.CLEAR: 	cmd = ButtonCommands.CLEAR; break;
		case ButtonIds.RESET: 	cmd = ButtonCommands.RESET; break;
		// Digit Buttons.
		case ButtonIds.DIGIT_0: cmd = ButtonCommands.DIGIT_ZERO; break;
		case ButtonIds.DIGIT_1: cmd = ButtonCommands.DIGIT_ONE; break;
		case ButtonIds.DIGIT_2: cmd = ButtonCommands.DIGIT_TWO; break;
		case ButtonIds.DIGIT_3: cmd = ButtonCommands.DIGIT_THREE; break;
		case ButtonIds.DIGIT_4: cmd = ButtonCommands.DIGIT_FOUR; break;
		case ButtonIds.DIGIT_5: cmd = ButtonCommands.DIGIT_FIVE; break;
		case ButtonIds.DIGIT_6: cmd = ButtonCommands.DIGIT_SIX; break;
		case ButtonIds.DIGIT_7: cmd = ButtonCommands.DIGIT_SEVEN; break;
		case ButtonIds.DIGIT_8: cmd = ButtonCommands.DIGIT_EIGHT; break;
		case ButtonIds.DIGIT_9: cmd = ButtonCommands.DIGIT_NINE; break;
		default:
			EventButton.sendPressedEvent(button.getID());
			return;
		}
		EventCommand.sendEvent(cmd);
	}

	@Override
	public void release(Button button) {
		int buttonId = button.getID();
		if (buttonId < ButtonIds.MIN_ID || buttonId > ButtonIds.MAX_ID) {
			EventButton.sendReleasedEvent(buttonId);
		}
	}

}
