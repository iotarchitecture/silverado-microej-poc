<?xml version="1.0"?>

<!--
	Front Panel

	Copyright 2021 MicroEJ Corp. All rights reserved.
	This Software has been designed or modified by MicroEJ Corp.
	MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
-->

<frontpanel
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="https://developer.microej.com"
	xsi:schemaLocation="https://developer.microej.com .widget.xsd">

	<device name="STM32F7508DK" skin="skin.png">
		<ej.fp.widget.Display x="30" y="30" width="480" height="272"/>

		<ej.fp.widget.Button label="0" x="28" y="314" skin="button-off.png" pushedSkin="button-a-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="1" x="150" y="314" skin="button-off.png" pushedSkin="button-a-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="2" x="272" y="314" skin="button-off.png" pushedSkin="button-a-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="3" x="394" y="314" skin="button-off.png" pushedSkin="button-a-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>

		<ej.fp.widget.Button label="4" x="28" y="418" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="5" x="150" y="418" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="6" x="272" y="418" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="7" x="394" y="418" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>

		<ej.fp.widget.Button label="8" x="28" y="504" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="9" x="150" y="504" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="10" x="272" y="504" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="11" x="394" y="504" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>

		<ej.fp.widget.Button label="12" x="28" y="590" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="13" x="150" y="590" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="14" x="272" y="590" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="15" x="394" y="590" skin="button-off.png" pushedSkin="button-b-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>

		<ej.fp.widget.Button label="16" x="28" y="676" skin="button-off.png" pushedSkin="button-c-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="17" x="150" y="676" skin="button-off.png" pushedSkin="button-c-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="18" x="272" y="676" skin="button-off.png" pushedSkin="button-c-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
		<ej.fp.widget.Button label="19" x="394" y="676" skin="button-off.png" pushedSkin="button-c-on.png" listenerClass="com.microej.sbd.framework.SbdButtonListener"/>
	</device>
</frontpanel>
