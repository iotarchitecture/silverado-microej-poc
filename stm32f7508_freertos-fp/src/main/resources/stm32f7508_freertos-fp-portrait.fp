<?xml version="1.0"?>

<!--
	Front Panel

	Copyright 2021 MicroEJ Corp. All rights reserved.
	This Software has been designed or modified by MicroEJ Corp.
	MicroEJ Corp. grants to SBD the non-exclusive right to freely use, modify and distribute this Software.
-->

<frontpanel
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="https://developer.microej.com"
	xsi:schemaLocation="https://developer.microej.com .widget.xsd">

	<device name="STM32F7508DK" skin="skin-portrait.png">
		<ej.fp.widget.Display x="17" y="17" width="272" height="480"/>
		<ej.fp.widget.Pointer x="17" y="17" width="272" height="480" touch="true"/>
	</device>
</frontpanel>
