/*
 * C
 *
 * Copyright 2021 MicroEJ Corp. All rights reserved.
 * This library is provided in source code for use, modification and test, subject to license terms.
 * Any modification of the source code will break MicroEJ Corp. warranties on the whole library.
 */
  .syntax unified
  .cpu cortex-m7
  .fpu softvfp
  .thumb
  
.text
.type rotation90, %function
.global rotation90
rotation90:
	PUSH {r3-r11,lr}

	SUBS r11,r1,#0x3FC00
rot90_loop:
	LDRH r3,[r0,#0x00]
	LDRH r4,[r0,#0x02]
	LDRH r5,[r0,#0x04]
	LDRH r6,[r0,#0x06]
	LDRH r7,[r0,#0x08]
	LDRH r8,[r0,#0x0A]
	LDRH r9,[r0,#0x0C]
	LDRH r10,[r0,#0x0E]

	STRH r3,[r1]
	SUBS r1,#0x3c0
	STRH r4,[r1]
	SUBS r1,#0x3c0
	STRH r5,[r1]
	SUBS r1,#0x3c0
	STRH r6,[r1]
	SUBS r1,#0x3c0
	STRH r7,[r1]
	SUBS r1,#0x3c0
	STRH r8,[r1]
	SUBS r1,#0x3c0
	STRH r9,[r1]
	SUBS r1,#0x3c0
	STRH r10,[r1]
	SUBS r1,#0x3c0

	CMP r1,r11
	BNE rot90_next
	ADDS r1,#0x3FC00
	ADDS r1,#0x02
	ADDS r11,#0x02

rot90_next:
	ADDS r0,r0,#0x10
	CMP r0,r2
	BNE rot90_loop
	POP {r3-r11,pc}
